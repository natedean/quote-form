import webpack from 'webpack';
import path from 'path';
import autoprefixer from 'autoprefixer';
import jsonImporter from 'node-sass-json-importer';

export default {
	debug: true,
	devtool: 'cheap-module-eval-source-map',
	noInfo: true,
	entry: {
		app:[
			'babel-polyfill',
			'eventsource-polyfill', // necessary for hot reloading with IE
			'webpack-hot-middleware/client?reload=true', //note that it reloads the page if hot module reloading fails.
			'whatwg-fetch',
			'./src/index'
		],
		vendor: [
			'lodash',
			'material-ui',
			'node-uuid',
			'react',
			'react-dom',
			'react-redux',
			'react-router',
			'react-router-redux',
			'react-scroll',
			'react-select',
			'react-tap-event-plugin',
			'redux',
			'redux-thunk',
			'whatwg-fetch',
			'xml2js',
			'xmlbuilder',
			'zipcodes'
		]
	},
	target: 'web',
	output: {
		path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './src'
	},
	plugins: [
		new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('test')}}),
		new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	],
	module: {
		loaders: [
			{test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel']},
			{test: /\.css$/, loader: "style!css"},
			{test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
			{test: /\.(png|jpe?g|gif|ttf|woff|woff2)$/, loader: 'url'},
			{test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
			{test: /\.scss$/, loaders: ['style', 'css?sourceMap', 'postcss', 'sass?sourceMap']},
			{test: /\.json$/, loader: 'json', include: path.join(__dirname, 'src')}
		]
	},
	sassLoader: {
		importer: jsonImporter
	},
	postcss: [autoprefixer({browsers: ['last 3 versions']})]
};
