import fs from 'fs-extra';
import cheerio from 'cheerio';
import colors from 'colors';

import GA from '../src/scriptTags/googleAnalytics';
import GAS from '../src/scriptTags/googleAnalyticsOnSteroids';
import scodeAndSiteCatalyst from '../src/scriptTags/scodeAndSiteCatalyst';

import hotjar from '../src/scriptTags/hotjar';
import remarketing from '../src/scriptTags/remarketing';
import quantcast from '../src/scriptTags/quantcast';
import uber from '../src/scriptTags/uber';
import adobeMarketing  from '../src/scriptTags/adobeMarketing';
import getDigitalData from '../src/scriptTags/digitalData';

/* eslint-disable no-console, no-var */
function lazyLoader(url, elementId) {
	var image = new Image(),
		height = 1,
		width = 1,
		border = 0,
		container = document.getElementById(elementId);

	image.onload = function () {
		container.appendChild(image);
	};
	image.src = url;
	image.height = height;
	image.width = width;
	image.border = border;
}

/**
 * @description
 * Copy selectquote shortcut icon into dist folder
 */
fs.copy('src/images/selectquote_favicon.ico', 'dist/selectquote.ico', function (err) {
	if (err) return console.error(err);
	console.log('selectquote.ico written to /dist'.green);
});

fs.readFile('src/index.html', 'utf8', (err, markup) => {
	if (err) {
		return console.log(err.red);
	}

	const $ = cheerio.load(markup);
	const $head = $('head');
	const $body = $('body');
	// STYLES
	$head.prepend('<link rel="stylesheet" href="styles.css">');

	//Shortcut icon
	$head.prepend('<link async rel="shortcut icon" href="selectquote.ico">');

	// HEAD TAGS
	$head.prepend('<script async rel="text/javascript">' + GA + ' GA();</script>');
	// TODO: Uncomment this tag once the GAS code is fixed
	// $head.prepend('<script rel="text/javascript">'+ GAS +' GAS();</script>');
	$head.append('<script async rel="text/javascript" src="//service.maxymiser.net/api/us/selectquote.com/30d35b/mmapi.js">');
	$head.append('<script async rel="text/javascript">' + getDigitalData + ' var digitalData = getDigitalData();</script>');
	$head.append('<script async rel="text/javascript">' + scodeAndSiteCatalyst + ' scodeAndSiteCatalyst();</script>');

	// BODY TAGS
	$body.append('<script async rel="text/javascript">' + hotjar + ' hotjar();</script>');
	$body.append('<script async rel="text/javascript">' + remarketing + ' remarketing();</script>');
	$body.append('<script type="text/javascript">' + lazyLoader + 'lazyLoader("//sqdevhome10.122.2O7.net/b/ss/sqdevhome10/1/H.20.3--NS/0", "omnit-holder-anchor");</script>');
	$body.append('<script type="text/javascript">' + lazyLoader + 'lazyLoader("//googleads.g.doubleclick.net/pagead/viewthroughconversion/1071628327/?value=0&amp;guid=ON&amp;script=0", "pixel-container");</script>');
	$body.append('<script type="text/javascript">' + lazyLoader + 'lazyLoader("//pixel.quantserve.com/pixel/p-b4LMiRprl_R4M.gif?labels=_fp.event.Default", "pixel-container");</script>');
	$body.append('<script async language="JavaScript" type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">');
	$body.prepend(
		'<span style="display:none;" id="pixel-container">' +
		'<a href="//www.omniture.com" title="Web Analytics" id="omnit-holder-anchor"></a>' +
		'</span>'
	);
	$body.append('<script async rel="text/javascript">' + quantcast + ' quantcast();</script>');
	$body.append('<script async rel="text/javascript">' + uber + ' uber();</script>');

	// let's keep these blocking tags below everything else
	$body.append('<script language="JavaScript" type="text/javascript" src="//assets.adobedtm.com/0516304ed27e11b56d17f1a1e81da2b4a08dfc21/satelliteLib-327091bf2e131cccb7ecaa2de7ee86cd9f9f638a.js">');
	$body.append('<script rel="text/javascript">' + adobeMarketing + ' adobeMarketing();</script>');

	fs.writeFile('dist/index.html', $.html(), 'utf8', function (err) {
		if (err) {
			return console.log(err.red);
		}

		console.log('index.html written to /dist'.green);
	});
});
