import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';
import jsonImporter from 'node-sass-json-importer';
import CompressionPlugin from 'compression-webpack-plugin';

const GLOBALS = {
	'process.env.NODE_ENV': process.env.NODE_ENV ? JSON.stringify(process.env.NODE_ENV) : JSON.stringify('production')
};

export default {
	debug: true,
	devtool: 'cheap-module-source-map',
	noInfo: false,
	entry: {
		app: [
			'babel-polyfill',
			'whatwg-fetch',
			'./src/index'
		],
		vendor: [
			'zipcodes',
			'whatwg-fetch',
			'node-uuid',
			'xmlbuilder',
			'xml2js'
		]
	},
	target: 'web',
	output: {
		path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './dist'
	},
	plugins: [
		new webpack.DefinePlugin(GLOBALS),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({
			mangle: true,
			compress: {
				warnings: false,
				pure_getters: true,
				unsafe: true,
				unsafe_comps: true,
				screw_ie8: true
			},
			output: {
				comments: false
			},
			exclude: [/\.min\.js$/gi]
		}),
		new CompressionPlugin({
			asset: '[path].gz[query]',
			algorithm: 'gzip',
			test: /\.js$|\.css$|\.html$/,
			threshold: 10240,
			minRation: 0.8
		}),
		new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js', {async: true}),
		new ExtractTextPlugin('styles.css')
	],
	module: {
		loaders: [
			{test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel']},
			{test: /\.css$/, loader: ExtractTextPlugin.extract('style', ['css-loader?sourceMap', 'postcss-loader'])},
			{test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
			{test: /\.(png|jpe?g|gif|woff|woff2)$/, loader: 'url?limit=2000'},
			{test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=2000&mimetype=application/octet-stream'},
			{test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=2000&mimetype=image/svg+xml'},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style', ['css-loader?sourceMap!sass', 'postcss-loader!sass'])
			},
			{test: /\.json$/, loader: 'json-loader', include: path.join(__dirname, 'src')}
		]
	},
	sassLoader: {
		importer: jsonImporter
	},
	postcss: [autoprefixer({browsers: ['last 3 versions']})]
};
