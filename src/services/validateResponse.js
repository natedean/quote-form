/**
 * @constant validateResponse
 * @type {function}
 * @param {function} validatorFunction - the funciton to be applied to the response
 * @return {function} - anonymous function that takes a response to be validated
 * @description
 * A function that returns a function which accepts a response parameter and validates it
 * by either throwing an error on failure or returning the response on success
 */
const validateResponse = validationFunction => response => {
    if (!validationFunction(response)) {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
    return response;
};

export default validateResponse;