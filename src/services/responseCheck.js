/**
 * @constant responseCheck
 * @type {function}
 * @param {object} response - the API response to be validated
 * @return {boolean} - true or false depending on status of the response
 * @description
 * A function to check the status of the response and return a boolean
 * based on whether the response has a successful status or not
 */
const responseCheck = response => response.status >= 200 && response.status < 300;

export default responseCheck;