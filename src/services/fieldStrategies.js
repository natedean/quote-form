import zipcodeChecker from './zipcodeChecker';
import validatorFactory from './validatorFactory';

const strategies = {
	zipcode: (zipcode, state) => {
		let errorTextCollection = Object.assign({}, state.errorTextCollection);
		let currentFormEntry = Object.assign({}, state.currentFormEntry);
		let zipResult = zipcodeChecker(zipcode);

		currentFormEntry.zipcode = zipcode;
		currentFormEntry.state = zipResult.zipcodeState;

		errorTextCollection.zipcode = (zipcode.length < 5) ? ''
			: zipResult.zipcodeErrorText;

		return Object.assign({}, state, {errorTextCollection}, {currentFormEntry});
	},
	phoneNumber: (phoneNumber, state) => {
		let errorTextCollection = Object.assign({}, state.errorTextCollection);
		let currentFormEntry = Object.assign({}, state.currentFormEntry);

		// 1.) Set the currentFormEntry's phoneNumber
		currentFormEntry.phoneNumber = phoneNumber;

		// 2.) Set our phoneNumber Error Text
		errorTextCollection.phoneNumber = ((phoneNumber.length === 0) ? ''
			: validatorFactory('phone')(currentFormEntry.phoneNumber).validate().errorMessage);

		return Object.assign({}, state, {errorTextCollection}, {currentFormEntry});
	},
	email: (email, state) => {
		let errorTextCollection = Object.assign({}, state.errorTextCollection);
		let currentFormEntry = Object.assign({}, state.currentFormEntry);

		currentFormEntry.email = email;
		errorTextCollection.email = ((email.length === 0) ? ''
			: validatorFactory('email')(email).validate().errorMessage);

		return Object.assign({}, state, {errorTextCollection}, {currentFormEntry});
	},
	firstName: (firstName, state) => {
		let errorTextCollection = Object.assign({}, state.errorTextCollection);
		let currentFormEntry = Object.assign({}, state.currentFormEntry);

		currentFormEntry.firstName = firstName;
		errorTextCollection.firstName = (firstName && /[^.]{2,}$/.test(firstName) == false)
			? 'Please enter a valid first name.' : '';

		return Object.assign({}, state, {errorTextCollection}, {currentFormEntry});
	},
	lastName: (lastName, state) => {
		let errorTextCollection = Object.assign({}, state.errorTextCollection);
		let currentFormEntry = Object.assign({}, state.currentFormEntry);

		currentFormEntry.lastName = lastName;
		errorTextCollection.lastName = (lastName && /[^.]{2,}$/.test(lastName) == false)
			? 'Please enter a valid last name.' : '';

		return Object.assign({}, state, {errorTextCollection}, {currentFormEntry});
	}
};

export default strategies;
