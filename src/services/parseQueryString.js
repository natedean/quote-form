/**
 * @function parseQueryString
 * @param {object} urlLocation
 * @description
 *   This function parses out any params in the url location and returns an object of them.
 * */
const parseQueryString = (urlLocation) => {
	let coverage = null;
	let term = null;
	let source = null;
	let cid = null;
	let tollFreeNumber = null;
	let trustPilotCustomers = null;
	let participant = null;

	if(urlLocation.query) {
		const {query} = urlLocation;
		coverage = query.coverage ? query.coverage : null;
		term = query.term ? query.term : null;
		source = query.source ? query.source : null;
		cid = query.cid ? query.cid : null;
		tollFreeNumber = (query.phone && /^1?\d{10}$/.test(query.phone)) ? query.phone : null;
		trustPilotCustomers = query.trustpilotcustomers ? query.trustpilotcustomers : null;
		participant = query.participant ? query.participant : null;
	}

	return {
		coverage,
		term,
		source,
		cid,
		tollFreeNumber,
		trustPilotCustomers,
		participant
	};
};

export default parseQueryString;
