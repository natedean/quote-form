import emailTLDTable from '../constants/emailTLDTable';
import {bindAll} from 'lodash';

const ErrorMessage = 'Please enter a valid email!';

/**
 * @class EmailValidator
 * @param {string} email - Email address to be validated
 * @description
 * A class used to validate email addresses
 */
class EmailValidator {
    /**
     * @function constructor
     * @description
     * Match the email address using a regex to enforce the format.
     * If all the pieces of the email address can't be matched, set the error message.
     * Also, bind all class methods to this instance of the class.
     */
    constructor(email) {
        this.email = email;
        if (this.email) {
            this.emailMatch = this.email.match(/^[a-zA-Z0-9.!#$%&â€™*+=?^_`{|}~-]+@([a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*)$/);
        }
        if (this.emailMatch) {
            this.domain = this.emailMatch[1];
        }
        if (this.domain) {
            this.topLevelDomainMatch = this.domain.match(/\.([^.]*)$/);
        }
        if (this.topLevelDomainMatch) {
            this.topLevelDomain = this.topLevelDomainMatch[1];
        }

        if (!this.topLevelDomain) {
            this.errorMessage = ErrorMessage;
        } else {
            this.errorMessage = '';
        }

        bindAll(
            this,
            'setErrorMessage',
            'validateDomain',
            'validateTopLevelDomain',
            'validate'
        );
    }

    /**
     * @function setErrorMessage
     * @description
     * Helper method for setting the error message and returning the current class instance
     */
    setErrorMessage() {
        this.errorMessage = ErrorMessage;
        return this;
    }

    /**
     * @function validateDomain
     * @description
     * Validate the domain portion of the email address
     */
    validateDomain() {
        if (this.errorMessage.length !== 0) {
            return this;
        }

        if (this.domain.substring(0,4).toUpperCase() === 'WWW.') {
            return this.setErrorMessage();
        }

        return this;
    }

    /**
     * @function setErrorMessage
     * @description
     * Validate the top level domain portion of the email address
     */
    validateTopLevelDomain() {
        if (this.errorMessage.length !== 0) {
            return this;
        }

        //Check if the top level domain is in our approved list
        if (!emailTLDTable[this.topLevelDomain.toUpperCase()]) {
            return this.setErrorMessage();
        }

        return this;
    }

    /**
     * @function validate
     * @description
     * Call all validation functions to validate the phone number
     */
    validate() {
        return this.validateDomain()
            .validateTopLevelDomain();
    }
}

export default EmailValidator;
