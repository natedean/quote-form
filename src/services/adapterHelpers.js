import months from '../entities/Months';
import policyValueFromInternal from './policyValueFromInternal';
import termLengthFromInternal from './termLengthFromInternal';

/**
 * @function adapterHelpers
 * @param {string} purpose - the reason you are submitting; getBestQuote or formSubmission
 * @param {object} entry - the currentFormEntry from the Store
 * @description
 *   This function can take in a purpose and currentFormEntry and output a
 *   properly formatted object.
 * @return {object} an object with formatted properties
 * */
const adapterHelpers = (purpose, entry) => {
	const purposes = {getBestQuote: 'getBestQuote', formSubmission: 'formSubmission'};
	/**
	 * @function formatDateOfBirth
	 * @description
	 *   This function can output a properly formatted date.
	 * @return {string} formatted date
	 * */
	const formatDateOfBirth = () => {
		const {birthMonth, birthYear, dayInMonth} = entry.birthdate;
		const formattedMonth = months[birthMonth] < 9 ? `0${months[birthMonth] + 1}`
			: `${months[birthMonth] + 1}`;
		const formattedDay = dayInMonth > 9 ? dayInMonth.toString() : `0${dayInMonth}`;

		if (purpose === purposes.formSubmission) {
			return `${formattedMonth}/${formattedDay}/${birthYear}`;
		}
		if (purpose === purposes.getBestQuote) {
			return `${birthYear}-${formattedMonth}-${formattedDay}`;
		}
	};

	/**
	 * @function formatGender
	 * @description
	 *   This function is responsible for returning a correctly formatted gender.
	 * */
	const formatGender = () => {
		if (purpose === purposes.formSubmission) {
			return entry.gender.substring(0, 1).toLowerCase();
		}
		if (purpose === purposes.getBestQuote) {
			if (entry.gender) {
				return (entry.gender.substring(0, 1).toLowerCase() === 'm') ? 112 : 113;
			} else {
				return '';
			}
		}
	};

	/**
	 * @function formatHasInsurance
	 * @description
	 *   This function formats a response to determine if the user has a policy or not.
	 * */
	const formatHasInsurance = () => {
		const userHasPolicy = entry.hasCurrentPolicy && entry.hasCurrentPolicy === 'hasPolicy';

		if (purpose === purposes.formSubmission) {
			return (userHasPolicy) ? 'y' : 'n';
		}
		if (purpose === purposes.getBestQuote) {
			return (userHasPolicy) ? true : false;
		}
	};

	/**
	 * @function formatHasUsedTobacco
	 * @description
	 *   This function formats a response indicating the use of tobacco products
	 * */
	const formatHasUsedTobacco = () => {
		const hasUsedTobacco = entry.hasUsedNicotine && entry.hasUsedNicotine === 'hasUsed';

		if (purpose === purposes.formSubmission) {
			return hasUsedTobacco ? 'c' : 'n';
		}
		if (purpose === purposes.getBestQuote) {
			return hasUsedTobacco ? true : false;
		}
	};

	/**
	 * @function formatDesiredAmount
	 * @description
	 *   This function formats a response for the desired loan amount
	 * */
	const formatDesiredAmount = () => {
		if (purpose === purposes.formSubmission) {
			return policyValueFromInternal(entry.policyValue).toFixed(0);
		}
		if (purpose === purposes.getBestQuote) {
			return policyValueFromInternal(entry.policyValue);
		}
	};

	/**
	 * @function formatDuration
	 * @description
	 *   This function formats a response indicating the requested duration of the loan
	 * */
	const formatDuration = () => {
		const duration = termLengthFromInternal(entry.termLength);

		if (purpose === purposes.formSubmission) {
			return duration.toFixed(0);
		}
		if (purpose === purposes.getBestQuote) {
			return duration;
		}
	};

	/**
	 * @function formatFeet
	 * @description
	 *   This function formats a response indicating the number of feet for a users height
	 * */
	const formatFeet = () => {
		if (purpose === purposes.formSubmission) {
			return (entry.height.length > 1)
				? entry.height.substring(0, entry.height.indexOf('.'))
				: entry.height;
		}
		if (purpose === purposes.getBestQuote) {
			return;
		}
	};

	/**
	 * @function formatInches
	 * @description
	 *   This function formats a response indicating the number of respective inches
	 * */
	const formatInches = () => {
		if (entry.height) {
			const thereAreInches = (entry.height.length > 1);
			const periodIndex = entry.height.indexOf('.');

			if (purpose === purposes.formSubmission) {
				return (thereAreInches) ? entry.height.substring(periodIndex + 1) : '0';
			}
			if (purpose === purposes.getBestQuote) {
				const feet = (thereAreInches) ? entry.height.substring(0, periodIndex) : entry.height;
				const inches = (thereAreInches) ? entry.height.substring(periodIndex + 1) : 0;

				return ((parseInt(feet) * 12) + parseInt(inches));
			}
		} else {
			return '';
		}
	};

	/**
	 * @function formatHealthRating
	 * @description
	 *   This function formats a response indicating health rating
	 * */
	const formatHealthRating = () => {
		if (purpose === purposes.formSubmission) {
			const healthRatingStrings = {
				1: 'Fair',
				2: 'Good',
				3: 'Very Good',
				4: 'Excellent'
			};
			return healthRatingStrings[entry.healthRating * 10] || 'Fair';
		}
		if (purpose === purposes.getBestQuote) {
			return entry.healthRating ? (5 - (entry.healthRating * 10)) : 4;
		}
	};

	return {
		dateOfBirth: formatDateOfBirth(),
		gender: formatGender(),
		hasInsurance: formatHasInsurance(),
		usedTobacco: formatHasUsedTobacco(),
		desiredAmount: formatDesiredAmount(),
		duration: formatDuration(),
		feet: formatFeet(),
		inches: formatInches(),
		healthRating: formatHealthRating()
	};
};

export default adapterHelpers;
