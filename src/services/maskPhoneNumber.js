/**
 * @constant DashFormatChars
 * @type {object}
 * @description
 * Object that holds dash format characters and locations within the string for those
 * characters to be used for formatting phone numbers
 */
const DashFormatChars = {
    3: '-',
    6: '-'
};

/**
 * @constant DotFormatChars
 * @type {object}
 * @description
 * Object that holds dot format characters and locations within the string for those
 * characters to be used for formatting phone numbers
 */
const DotFormatChars = {
	3: '.',
	6: '.'
};

/**
 * @function maskPhoneNumber
 * @param {string} phoneNumber - The phone number to be masked
 * @description
 * Service for formatting phone numbers
 */
const maskPhoneNumber = (phoneNumber, dashes = true) => {
	const formatChars = dashes ? DashFormatChars : DotFormatChars;
	const flatPhoneNumber = phoneNumber.replace(/\D/g, '');
	if (flatPhoneNumber.length === 11 && flatPhoneNumber[0] === '1') {
		return `1${dashes ? '-' : '.'}${flatPhoneNumber.substring(1)
			.split('')
			.reduce((curr, next, index) => `${curr}${formatChars[index] || ''}${next}`, '')}`;
	} else {
		return flatPhoneNumber.substring(0, 10)
			.split('')
			.reduce((curr, next, index) => `${curr}${formatChars[index] || ''}${next}`, '');
	}
};

export default maskPhoneNumber;
