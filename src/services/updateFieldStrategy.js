import strategies from './fieldStrategies';

const updateFieldStrategy = (name, fieldValue, state) => {
	if (name in strategies) {
		return strategies[name](fieldValue, state);
	} else {
		const healthRatingChanged = (name === 'healthRating') ? true : false;
		const currentFormEntry = Object.assign({}, state.currentFormEntry);
		currentFormEntry[name] = fieldValue;
		return Object.assign({}, state, {currentFormEntry, healthRatingChanged});
	}
};

export default updateFieldStrategy;
