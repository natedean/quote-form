import adapterHelpers from './adapterHelpers';

/**
 * @function getBestQuoteAdapter
 * @param {object} appState
 * @description
 *   This function takes in the state.App namespace from the Store state and converts it into a format
 *   Brett's getBestQuoteService can parse!
 * @returns {object} adaptedGetBestQuote
 * */
const entryAdapter = (appState) => {
	const {currentFormEntry} = appState;
	const {
		dateOfBirth, hasInsurance, gender, usedTobacco,
		desiredAmount, inches, duration, healthRating
	} = adapterHelpers('getBestQuote', currentFormEntry);

	return {
		FaceAmount: desiredAmount,
		Birthdate: dateOfBirth,
		ZIP: currentFormEntry.zipcode,
		Sex: gender,
		Duration: duration,
		Replacing: hasInsurance,
		Height: inches,
		Weight: currentFormEntry.weight ? currentFormEntry.weight : '',
		HealthRating: healthRating,
		IsUsedTobacco: usedTobacco
	};
};

export default entryAdapter;
