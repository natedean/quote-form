/**
 * @const sliderValues
 * @description
 * Our PolicyValue slider components need to step at irregular intervals, this array stores the stepped values we need.
 */
const sliderValues = [
	0.01,
	0.025,
	0.05,
	0.075,
	0.1,
	0.125,
	0.15,
	0.175,
	0.2
];

export const sliderMin = 0;
export const sliderMax = sliderValues.length - 1;
export const sliderStep = 1;

/**
 * @function getSliderValue
 * @param {float} policyValue - The policy value that needs to be mapped to a slider value
 * @return {int} - internal slider value
 * @description
 * Translate policyValue to sliderValue
 */
export const getSliderValue = policyValue => sliderValues.indexOf(policyValue);

/**
 * @function getPolicyValue
 * @param {int} sliderValue - The internal slider value
 * @return {float} - The corresponding policyValue from the dictionary set above
 * @description
 * Translate sliderValue to policyValue
 */
export const getPolicyValue = sliderValue => sliderValues[sliderValue];
