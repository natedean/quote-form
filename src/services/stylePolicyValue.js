/**
 * @function stylePolicyValue
 * @param {number} policyValue - The policy value as a dollar amount
 * @return {string} - The policy value styled with thousands commas
 * @description
 * Converts policy value to a styled string using thousands commas to make it easier to read.
 */
const stylePolicyValue =
	(policyValue) => policyValue.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export default stylePolicyValue;
