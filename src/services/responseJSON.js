const responseJSON = response => response.json();

export default responseJSON;
