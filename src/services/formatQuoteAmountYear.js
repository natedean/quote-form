function formatQuoteAmountYear(quoteAmount) {
    if(quoteAmount) {
		let styledQuoteAmount = quoteAmount.toFixed(2);
		const formatAtOneAndFour = styledQuoteAmount.length % 4 != 0;
		if (styledQuoteAmount.length > 6) {

			styledQuoteAmount = styledQuoteAmount.slice(0, formatAtOneAndFour ? 1 : 2) + ',' +
				styledQuoteAmount.slice(formatAtOneAndFour ? 1 : 2, styledQuoteAmount.length);
		}

		return styledQuoteAmount;
	}
	return quoteAmount;
}

export default formatQuoteAmountYear;
