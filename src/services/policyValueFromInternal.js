import policyValueConstants from '../constants/policyValueConstants';

/**
 * @function policyValueFromInternal
 * @param {number} policyValue - The policy value in internal format
 * @return {number} - The policy value as a dollar amount
 * @description
 * Converts policy value from our internal format to a dollar amount.
 */
const policyValueFromInternal =
	(policyValue) => Math.round(policyValue * policyValueConstants.conversionFactor);

export default  policyValueFromInternal;
