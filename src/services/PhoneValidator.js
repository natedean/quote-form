import restrictedAreaCodeTable from '../constants/restrictedAreaCodeTable';
import validAreaCodeTable from '../constants/validAreaCodeTable';
import {bindAll} from 'lodash';

const ErrorMessage = 'Please enter a valid phone number!';

/**
 * @class PhoneValidator
 * @param {string} phoneNumber - Phone number to be validated
 * @description
 * A class used to validate phone numbers
 */
class PhoneValidator {
    /**
     * @function constructor
     * @description
     * Flatten the phone number if it exists. If the phone number
     * does not exist or the flattened phone number does not exist,
     * set the error message.
     * Also, bind all class methods to this instance of the class.
     */
    constructor(phoneNumber) {
        this.phoneNumber = phoneNumber;
        if (this.phoneNumber) {
            this.flatPhoneNumber = this.phoneNumber.replace(/\D/g, '');
        }

        if (!this.flatPhoneNumber) {
            this.errorMessage = ErrorMessage;
        } else {
            this.errorMessage = '';
        }

        bindAll(
            this,
            'setErrorMessage',
            'validatePhoneNumberFormat',
            'validateNotRepeating',
            'validateAreaCode',
            'validate'
        );
    }

    /**
     * @function setErrorMessage
     * @description
     * Helper method for setting the error message and returning the current class instance
     */
    setErrorMessage() {
        this.errorMessage = ErrorMessage;
        return this;
    }

    /**
     * @function validatePhoneNumberFormat
     * @description
     * Validate rules for the format of the phone number
     */
    validatePhoneNumberFormat() {
        if (this.errorMessage.length !== 0) {
            return this;
        }

        if (this.flatPhoneNumber.length !== 10) {
            return this.setErrorMessage();
        }

        if (this.flatPhoneNumber[0] === '0' || this.flatPhoneNumber[0] === '1') {
            return this.setErrorMessage();
        }

        if (this.flatPhoneNumber[1] === '9') {
            return this.setErrorMessage();
        }

        if (this.flatPhoneNumber[1] === this.flatPhoneNumber[2]) {
            return this.setErrorMessage();
        }

        if (this.flatPhoneNumber[3] === '0' || this.flatPhoneNumber[3] === '1') {
            return this.setErrorMessage();
        }

        return this;
    }

    /**
     * @function validateNotRepeating
     * @description
     * Validate the phone number is not the same digit repeated 10 times
     */
    validateNotRepeating() {
        if (this.errorMessage.length !== 0) {
            return this;
        }

        if (/^([0-9])\1+$/.test(this.flatPhoneNumber)) {
            return this.setErrorMessage();
        }

        return this;
    }

    /**
     * @function validateAreaCode
     * @description
     * Validate the area code is a valid US area code and
     * that the area code is not in the list of blacklisted area
     * codes from Propulsion
     */
    validateAreaCode() {
        if (this.errorMessage.length !== 0) {
            return this;
        }

        const areacode = this.flatPhoneNumber.substring(0,3);
        if (restrictedAreaCodeTable[areacode]) {
            return this.setErrorMessage();
        }

        if (!validAreaCodeTable[areacode]) {
            return this.setErrorMessage();
        }

        return this;
    }

    /**
     * @function validate
     * @description
     * Call all validation functions to validate the phone number
     */
    validate() {
        return this.validatePhoneNumberFormat()
            .validateNotRepeating()
            .validateAreaCode();
    }
}

export default PhoneValidator;
