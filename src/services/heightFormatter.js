/*
 * @function heightFormatter
 * @param {number} lowerBound
 * @param {number} upperBound
 * @return {array} heightRange
 * @description
 *   This little helper takes in an inclusive lower bound
 *   and and exclusive upper bound
 * @example
 *   // This would return an array with a range from 4 ft to 7ft 11in
 *   const heightRange = heightFormatter(4,8);
 * */
const heightFormatter = (lowerBound, upperBound) => {

	let heightRange = [];
	let foot = lowerBound;
	const inches = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

	while (foot < upperBound) {
		heightRange.push({label: `${foot}'`, value: `${foot}`});

		if (foot !== upperBound) {
			inches.forEach(inch => {
				heightRange.push({
					label: `${foot}' ${inch}"`,
					value: `${foot}.${inch}`
				});
			});
		}

		++foot;
	}

	return heightRange;
};

export default heightFormatter;
