import {bindAll, forEach, valuesIn, omit} from 'lodash';

/**
 *    @class ProgressChecker
 *    @param {object} state
 *    @description
 *      This Class is responsible for checking the progress a user has made on the Form.
 * */
class ProgressChecker {
	constructor(state) {
		this.state = state;
		this.entry = state.currentFormEntry;
		this.errors = state.errorTextCollection;
		this.incompleteFields = state.incompleteFields;
		bindAll(this, [
			'checkFormCompletion',
			'checkBasicInfo',
			'checkHealthInfo',
			'checkContactInfo',
			'evaluateField',
			'fieldIsValid'
		]);
	}

	/**
	 * @function checkFormCompletion
	 * @returns {object} new props for state
	 * @description
	 *   This is the MAIN method in this class that kicks off the complete evaluation of the form progress
	 *   It returns out a new object which contains updated props that get added to the reducer state.
	 * */
	checkFormCompletion() {
		this.checkBasicInfo().checkHealthInfo().checkContactInfo();
		const isEntryReadyToSubmit = (this.thereAreNoErrors(this.errors) && this.theFormIsFilledOut(this.entry));
		const {incompleteFields, state} = this;
		return Object.assign({}, state, {isEntryReadyToSubmit, incompleteFields});
	}

	checkBasicInfo() {
		forEach({zipcode: 5, hasCurrentPolicy: 1, birthdate: 1, gender: 1}, this.evaluateField);
		return this;
	}

	checkHealthInfo() {
		forEach({healthRating: 1, hasUsedNicotine: 1, height: 1, weight: 1}, this.evaluateField);
		return this;
	}

	checkContactInfo() {
		forEach({firstName: 2, lastName: 2, email: 9, phoneNumber: 10}, this.evaluateField);
		return this;
	}

	evaluateField(value, key) {
		return (!this.fieldIsValid(key, value))
			? this.incompleteFields[key] = key
			: this.incompleteFields = omit(this.incompleteFields, key);
	}

	fieldIsValid(fieldName, minLength = 1) {
		return (fieldName === 'birthdate') ? (this.entry.birthdate && Object.keys(this.entry.birthdate).length === 3)
			: (this.entry[fieldName] && this.entry[fieldName].toString().length >= minLength)
		&& (!this.errors[fieldName]);
	}

	theFormIsFilledOut(entry) {
		return valuesIn(entry).every(entryField => (entryField !== null && entryField.toString().length > 0))
			&& Object.keys(entry.birthdate).length === 3
			&& valuesIn(entry.birthdate).every(entryField => (entryField !== null && entryField.toString().length > 0));
	}

	thereAreNoErrors(errors) {
		return valuesIn(errors).every(error => (!error));
	}
}

export default ProgressChecker;
