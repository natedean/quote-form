import termLengthConstants from '../constants/termLengthConstants';

/**
 * @function termLengthFromInternal
 * @param {number} termLength - The term length in internal format
 * @return {string} - The term length in years
 * @description
 * Converts term length value from our internal format to years.
 */
const termLengthFromInternal =
	(termLength) => Math.round(termLength * termLengthConstants.conversionFactor);

export default termLengthFromInternal;
