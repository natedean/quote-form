import progressTypes from './userProgressTypes';

/**
 * @function userProgressService
 * @param {array} type - of section user needs progress status on
 * @param {object} incompleteFields
 * @param {object} currentFormEntry
 * @description
 * check progressTypes against currentFormEntry and return the users progress
 */
const userProgressService = (type, incompleteFields, currentFormEntry) => {
	let progress;
	if (['health','basic','contact'].indexOf(type) > -1) {
		progress = progressTypes[type].reduce((currentProgress, key) => {
			if (currentFormEntry[key]) {
				currentProgress.userProgress[key] = key;
			}
			if (incompleteFields[key]) {
				currentProgress.incompleteFields[key] = incompleteFields[key];
			}

			return currentProgress;
		}, { userProgress: {}, incompleteFields: {} });
	}
	// Default return of undefined if an invalid type specified.
	return progress;
};

export default userProgressService;
