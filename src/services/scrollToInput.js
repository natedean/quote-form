import scroll from 'react-scroll/lib/mixins/animate-scroll';
import constants from '../constants/scrollToConstants';
import isMobile from './isMobileCheck';

/**
 * @function scrollToInput
 * @param {object} event - an event from the user that should trigger a scroll
 * @param {string} [event.default.name] - The name of field from event (used for dropdowns)
 * @param {string} [event.target.name] - The name of field from event (used for radios and text inputs)
 * @param {number} [event.target.offsetParent.offsetTop] - Gives you the offsetTop number from input
 * @description
 * This service is to add a scrollTo function for mobile so that the input
 * or dropdown will always be in focus for the user.
 */
const scrollToInput = (event) => {
	const currentWindowWidth = window.innerWidth;
	const windowSizeBreakpoint = constants.windowSizeBreakpoint;
	const isApple = isMobile.iOS();
	let offsetTop, eventName, offsetReduction;

	if (currentWindowWidth < windowSizeBreakpoint) {
		if (event.default) {
			eventName = event.default.name;
			offsetTop = document.getElementsByClassName(eventName)[0].offsetTop;
			offsetReduction = constants.inputs[eventName].offsetReduction;
			const shouldBeScrolled = (window.pageYOffset !== offsetTop - offsetReduction);

			if (shouldBeScrolled || offsetReduction) return scroll.scrollTo(offsetTop - offsetReduction);
		} else if (!isApple) {
			eventName = event.target.name;
			offsetTop = event.target.offsetParent.offsetTop;
			offsetReduction = constants.inputs[eventName].offsetReduction;
			const shouldBeScrolled = (window.pageYOffset !== offsetTop - offsetReduction);

			if (shouldBeScrolled || offsetReduction) return scroll.scrollTo(offsetTop - offsetReduction);
		}
	}
};

export default scrollToInput;
