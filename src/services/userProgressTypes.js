const userProgressTypes = {
	basic: ['zipcode','birthdate','hasCurrentPolicy','gender'],
	health: ['healthRating', 'height', 'weight', 'hasUsedNicotine'],
	contact: ['firstName', 'lastName', 'phoneNumber', 'email']
};

export default userProgressTypes;
