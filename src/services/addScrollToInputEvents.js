import scrollToInput from './scrollToInput';

/**
 * @function addScrollToInputEvents
 * @description
 * Add onClick event listener to all input fields and set
 * their scroll position for the scrollToInput service
 */
const addScrollToInputEvents = () => {
	const formInputs = Array.from(document.querySelectorAll('input'));

	const focusField = (event) => {
		const objectArgs = inputEvents[event.target.name];
		scrollToInput(objectArgs);
	};

	const inputEvents = formInputs.reduce((acc, inputElement) => {
		acc[inputElement.name] = {target: {name: inputElement.name, offsetParent: inputElement.parentElement}};
		return acc;
	}, {});

	formInputs.forEach(inputElement => inputElement.addEventListener('click', focusField));
};

export default addScrollToInputEvents;
