import termLengthConstants from '../constants/termLengthConstants';

/**
 * @function termLengthToInternal
 * @param {number} termLength - The term length in years
 * @return {number} - The term length in our internal format
 * @description
 * Converts term length from years to our internal format.
 */
const termLengthToInternal =
	(termLength) => termLength / termLengthConstants.conversionFactor;

export default termLengthToInternal;
