import responseCheck from './responseCheck';
import validateResponse from './validateResponse';
import xmlbuilder from 'xmlbuilder';
import xml2js from 'xml2js';

/**
 * @constant xmlToJSON
 * @type {function}
 * @param {string} xmlString - XML string to be converted to JSON
 * @return {promise} - promise that resolves to the converted JSON or rejects with a parsing error
 * @description
 * A function that takes in an XML string to be converted to JSON and returns a promise that will resolve to
 * the converted JSON object or reject with an error that occurred during the parsing
 */
const xmlToJSON = xmlString => {
	return new Promise((resolve, reject) => {
		xml2js.parseString(xmlString, {explicitArray: false}, (err, result) =>
			err ? reject(err) : resolve(result)
		);
	});
};

/**
 * @constant formSubmissionService
 * @type {function}
 * @param {object} leadJSON - the JSON object to be POSTed
 * @return {promise} - promise that resolves to the API response as a JSON object
 * @description
 * A function that takes in a JSON object to be POSTed and returns a promise that will
 * resolve to the response from the API as a JSON object
 */
const formSubmissionService = (leadJSON) => {
	leadJSON.login.userid = 'SQSenior';
	leadJSON.login.password = 'nt469Plh56Fv';

	const serviceRoute = (process.env.NODE_ENV !== 'production') ?
		'https://webchat.selectquote.com/services/form/?env=dev' :
		'https://www.selectquote.com/services/form/?env=prod';

	const xmlData = xmlbuilder.create('request', {headless: true})
		.ele(leadJSON)
		.end();
	return fetch(serviceRoute, {
		method: 'POST',
		mode: 'cors',
		headers: {
			'Content-Type': 'text/xml'
		},
		body: xmlData
	})
		.then(validateResponse(responseCheck))
		.then(response => response.text())
		.then(xmlToJSON);
};

export default formSubmissionService;
