import policyValueConstants from '../constants/policyValueConstants';

/**
 * @function policyValueToInternal
 * @param {number} policyValue - The policy value in dollars
 * @return {number} - The policy value in our internal format
 * @description
 * Converts policy value from a dollar amount to our internal format.
 */
const policyValueToInternal =
	(policyValue) => policyValue / policyValueConstants.conversionFactor;

export default policyValueToInternal;
