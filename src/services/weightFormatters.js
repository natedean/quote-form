const weightFormatter = (lowerBound, upperBound) => {

    let poundCount = lowerBound;
    let weightRange = [];

    while (poundCount <= upperBound) {
        weightRange.push({label: poundCount+ ' lbs', value: poundCount});
        ++poundCount;
    }

    return weightRange;
};

export default weightFormatter;