import responseCheck from './responseCheck';
import validateResponse from './validateResponse';
import responseJSON from './responseJSON';

//This subscription key was provided by Sergey and should not be subject to Quota limits
const OcpApimSubscriptionKey = '390ff5f2abeb4b40a2711590adfedb16';
const DefaultHeaders = {
	'Ocp-Apim-Subscription-Key': OcpApimSubscriptionKey
};

/**
 * @constant validateQuoteRequestJSON
 * @type {function}
 * @param {object} quoteRequestJSON - the JSON object to be validated
 * @description
 * A function that takes in a JSON object to be validated and throws an error if it
 * fails validation
 */
const validateQuoteRequestJSON = quoteRequestJSON  => {
	if (typeof(quoteRequestJSON.ZIP) !== 'string' || quoteRequestJSON.ZIP.length === 0) {
		throw new TypeError('[ZIP] is required to receive a quote.');
	}
	if (typeof(quoteRequestJSON.Birthdate) !== 'string' || quoteRequestJSON.Birthdate.length === 0) {
		throw new TypeError('[Birthdate] is required to receive a quote.');
	}
	if (typeof(quoteRequestJSON.FaceAmount) !== 'number') {
		throw new TypeError('[FaceAmount] is required to receive a quote.');
	}
};

/**
 * @constant fetchAPIKey
 * @type {function}
 * @return {promise} - promise that resolves to the API response containing the API key
 * @description
 * A function that returns a promise that will resolve to the API response containing the API key
 */
const fetchAPIKey = () => {
	const headers = new Headers(DefaultHeaders);
	return fetch('https://sqapi.azure-api.net/SQAPIKey/SQCalc', {
		method: 'POST',
		mode: 'cors',
		headers: headers
	});
};

/**
 * @constant extractAPIKey
 * @type {function}
 * @param {object} apiKeyJSON - the JSON object containing the API key
 * @return {string} - the API key as a string
 * @description
 * A function that takes in a JSON object containing the API key and returns the API key as a string
 */
const extractAPIKey = apiKeyJSON => {
	if (typeof(apiKeyJSON.SQAPIKey) !== 'string' || apiKeyJSON.SQAPIKey.length === 0) {
		throw new Error('[SQAPIKey] retrieval failed.');
	}
	return apiKeyJSON.SQAPIKey;
};

/**
 * @constant fetchBestQuote
 * @type {function}
 * @param {object} quoteRequestJSON - the JSON request object
 * @return {function}
 * @description
 * A function that takes in a JSON request object and returns a function that takes
 * the API key and will return a promise that will resolve to the response from the
 * quote API
 */
const fetchBestQuote = quoteRequestJSON => apiKey => {
	const body = JSON.stringify([quoteRequestJSON]);
	let headers = new Headers(DefaultHeaders);
	headers.append('SQAPIKey', apiKey);
	headers.append('Content-Type', 'application/json');
	return fetch('https://sqapi.azure-api.net/SQCalc/GetBestQuote', {
		method: 'POST',
		mode: 'cors',
		headers,
		body
	});
};

/**
 * @constant extractBestQuote
 * @type {function}
 * @param {object} bestQuoteJSON - the JSON object containing the best quote
 * @return {object} - the JSON object representation of the best quote
 * @description
 * A function that takes in a JSON object containing the best quote
 * and returns a JSON object representation of the best quote
 */
const extractBestQuote = bestQuoteJSON => {
	const errorString = '[bestQuoteJSON] is not of the format [[{}]].';
	if (!Array.isArray(bestQuoteJSON) || bestQuoteJSON.length === 0) {
		throw new TypeError(errorString);
	}
	if (!Array.isArray(bestQuoteJSON[0]) || bestQuoteJSON[0].length === 0) {
		throw new TypeError(errorString);
	}
	if (typeof(bestQuoteJSON[0][0]) !== 'object' || bestQuoteJSON[0][0] === null) {
		throw new TypeError(errorString);
	}
	return bestQuoteJSON[0][0];
};

/**
 * @constant getBestQuoteService
 * @type {function}
 * @param {object} quoteRequestJSON - the JSON object used to build the GET URL
 * @return {promise} - promise that resolves to the quote API response as a JSON object
 * @description
 * A function that takes in a JSON object used to build the GET URL and returns a promise that will
 * resolve to the response from the quote API as a JSON object
 */
const getBestQuoteService = quoteRequestJSON =>
	Promise.resolve(quoteRequestJSON)
		.then(validateQuoteRequestJSON)
		.then(fetchAPIKey)
		.then(validateResponse(responseCheck))
		.then(responseJSON)
		.then(extractAPIKey)
		.then(fetchBestQuote(quoteRequestJSON))
		.then(validateResponse(responseCheck))
		.then(responseJSON)
		.then(extractBestQuote);

export default getBestQuoteService;
