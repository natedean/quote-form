import zipcodes from 'zipcodes';

const zipcodeChecker = (zipcode) => {
	let lookupResponse = zipcodes.lookup(zipcode);
	return {
		zipcodeErrorText: zipcodeErrorText(zipcode, lookupResponse),
		zipcodeState: zipcodeState(lookupResponse)
	};
};

const zipcodeErrorText = (zipcode, lookupResponse) => {
    return (/^\d{5}?$/.test(zipcode) == false || typeof(lookupResponse) === 'undefined') || lookupResponse.state === 'SD'
        ? 'SelectQuote coverage is not available in South Dakota or outside the United States.'
        : '';
};

const zipcodeState = (lookupResponse) => {
	return (lookupResponse && lookupResponse.state) ? lookupResponse.state : '';
};

export default zipcodeChecker;
