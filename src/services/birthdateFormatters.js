import Months from '../entities/Months';
import DaysInMonth from '../entities/DaysInMonth';

let currentDate = new Date(Date.now());
let currentYear = currentDate.getFullYear();
let currentMonth = currentDate.getMonth();
let currentDayInMonth = currentDate.getDate();
let startYear = currentYear - 79;
let endYear = currentYear - 17;
let yearRange = buildYearRange(startYear, endYear);
let monthRange = buildMonthRange(Months);
let dayRange = buildDayRange('JANUARY');

export function getBirthdateInputValues(birthdate = null) {

    if(birthdate) {
        return appendBirthdateInputValues(birthdate);
    }

    return {
        formattedMonths: monthRange,
        formattedDays: dayRange,
        formattedYears: yearRange
    };
}

export function appendBirthdateInputValues(birthdate) {
    let age = (birthdate && birthdate.birthYear) ? currentYear - parseInt(birthdate.birthYear) : 80;
    let birthMonth = birthdate && birthdate.birthMonth ? birthdate.birthMonth : null;
    let newMonthRange = monthRange.slice();
    let newDayRange = dayRange.slice();

    if (age < 19) {
        newMonthRange = monthRange.slice(0, currentMonth + 1);

        if(currentMonth === monthRange.findIndex(x => x.value === birthMonth)) {
            newDayRange = dayRange.slice(0, currentDayInMonth);
        }
    }

    return {
        formattedMonths: newMonthRange,
        formattedDays: newDayRange,
        formattedYears: yearRange
    };
}

function buildYearRange(startYear, endYear) {

    let yearCounter = startYear;

    let range = [];
    while (range.length < (endYear - startYear)) {
        range.push({value: yearCounter, label: yearCounter});
        ++yearCounter;
    }

    return range;
}

function buildMonthRange(months) {

    let newMonths = [];
    Object.keys(months).forEach(month => {
        newMonths.push({value: month, label: (month.charAt(0).toUpperCase() + month.slice(1).toLowerCase())});
    });

    return newMonths;
}

function buildDayRange(currentMonth = 'JANUARY') {

    let daysInMonth = DaysInMonth[Months[currentMonth]] || 31;
    let days = [];
    let counter = 1;
    while (daysInMonth > days.length) {
        days.push({value: counter, label: counter});
        ++counter;
    }

    return days;
}