/**
 * @const {function} validateToInternal
 * @param value {number} - Number we want to test against
 * @param min {number} - Minimal number it must be greater than
 * @param max {number} - Max number it must be less than
 * @param multipleOf {number} - Number it must multiple by
 * @return {boolean}
 * @description
 * Test value we want to import internally.
 */
const validateToInternal = (value, rangeConstParams) => {
	const {min, max, multipleOf} = rangeConstParams;
	return (isNaN(value) || (value < min) || (value > max)) ? false : (value % multipleOf === 0);
};

export default validateToInternal;
