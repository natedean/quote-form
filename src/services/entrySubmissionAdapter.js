import adapterHelpers from './adapterHelpers';

/**
 * @function entryAdapter
 * @param {object} appState
 * @description
 *   This function takes in the state.App namespace from the Store state and converts it into a format
 *   Brett's formSubmissionService can parse!
 * @returns {object} adaptedCurrentEntry
 * */
const entryAdapter = (appState) => {
	const {
		currentFormEntry, selectedDuration, insCoName,
		quoteAmountMonth, policyName, source, cid, participant

	} = appState;
	const {
		dateOfBirth, hasInsurance, gender, usedTobacco,
		desiredAmount, inches, feet, duration, healthRating
	} = adapterHelpers('formSubmission', currentFormEntry);

	const adaptedCurrentEntry = {
		login: {
			action: 'insert',
			sourcecode: source || 'ZAMS',
			email: 'errors@propulsionlabs.com'
		},
		lead: {
			dateofbirth: dateOfBirth,
			gender,
			existinginsurance: hasInsurance,
			desiredamount: desiredAmount,
			duration,
			first: currentFormEntry.firstName,
			last: currentFormEntry.lastName,
			state: currentFormEntry.state,
			zip: currentFormEntry.zipcode,
			phone: currentFormEntry.phoneNumber.replace(/(-)/g, ''),
			email: currentFormEntry.email,
			heightfeet: feet,
			heightinches: inches,
			weight: currentFormEntry.weight.toString(),
			usetobacco: usedTobacco,
			healthrating: healthRating,
			//TODO When the results grid is available, this section will be based on the user's selection
			selectedterm: selectedDuration,
			selectedcoverage: desiredAmount,
			selectedcarrier: insCoName,
			selectedperiod: 'Monthly',
			selectedpremium: typeof(quoteAmountMonth) === 'number' ? quoteAmountMonth.toFixed(2).toString() : '0.00',
			selectedpolicy: policyName
		}
	};

	if(cid) {
		adaptedCurrentEntry.login.cid = cid;
	}

	if(participant) {
		adaptedCurrentEntry.login.participantid = participant;
	}

	return adaptedCurrentEntry;
};

export default entryAdapter;
