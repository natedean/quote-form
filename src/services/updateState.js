import updateFieldStrategy from './updateFieldStrategy';
import ProgressChecker from './ProgressChecker';
import progressService from './userProgressService';

/**
 * @function updateState
 * @param {object} event
 * @param {object} state
 * @returns {object} a new copy of the latest Reducer state
 * @description
 *   This function is responsible for updated the Reducer State
 * */
const updateState = (event, state) => {
	const {name, value} = event.target;
	const {currentSection, userClickedNextButton} = state;
	const sections = ['basic', 'health', 'contact'];
	const newState = updateFieldStrategy(name, value, state);
	const progress = new ProgressChecker(newState).checkFormCompletion();
	const {incompleteFields} = progressService(sections[currentSection], progress.incompleteFields, newState.currentFormEntry);
	const showCurrentSectionErrors = userClickedNextButton && Object.keys(incompleteFields).length > 0;

	return Object.assign({}, state, newState, progress, {showCurrentSectionErrors});
};

export default updateState;
