import EmailValidator from './EmailValidator';
import PhoneValidator from './PhoneValidator';

/**
 * @constant validatorFactory
 * @type {function}
 * @param {string} type
 * @description
 * Factory for returning a Validator of the specified type
 */
const validatorFactory = (type) => {
    if(type === 'email') {
        return (email) => new EmailValidator(email);
    }

    if(type === 'phone') {
        return (phoneNumber) => new PhoneValidator(phoneNumber);
    }
};

export default validatorFactory;