import policyValueToInternal from './policyValueToInternal';
import policyValueConstants from '../constants/policyValueConstants';
import termLengthToInternal from './termLengthToInternal';
import termLengthConstants from '../constants/termLengthConstants';
import validateToInternal from './validateToInternal';

/**
 * @function pushQueryPropsToState
 * @param {object} queryProps
 * @param {object} actions
 * @desciption
 *   This service handles pushing various queryParams to state.
 * */
const pushQueryPropsToState = (queryProps, actions) => {
	if (queryProps.coverage && validateToInternal(queryProps.coverage, policyValueConstants)) {
		actions.policyValueChanged(policyValueToInternal(queryProps.coverage));
	}

	if (queryProps.term && validateToInternal(queryProps.term, termLengthConstants)) {
		actions.termLengthChanged(termLengthToInternal(queryProps.term));
	}

	if (queryProps.source) {
		actions.sourceChanged(queryProps.source);
	}

	if (queryProps.cid) {
		actions.cidChanged(queryProps.cid);
	}

	if(queryProps.tollFreeNumber) {
		actions.tollFreeNumberChanged(queryProps.tollFreeNumber);
	}

	if (queryProps.trustPilotCustomers) {
		actions.trustPilotCustomersChanged(queryProps.trustPilotCustomers);
	}

	if (queryProps.participant) {
		actions.participantChanged(queryProps.participant);
	}
};

export default pushQueryPropsToState;
