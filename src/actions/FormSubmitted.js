import Types from './AppTypes';
import formSubmissionService from '../services/formSubmissionService';
import entryAdapter from '../services/entrySubmissionAdapter';
import getBestQuoteService from '../services/getBestQuoteService';
import getBestQuoteAdapter from '../services/getBestQuoteAdapter';

/**
 * @function FormSubmitted
 * @description
 * Checks to see if there are any errors on the form and if not attempt to get a quote
 * and submit the form
 * */
const FormSubmitted = () => {
	return (dispatch, getState) => {
		if (!getState().App.isEntryReadyToSubmit) {
			return dispatch({
				type: Types.UPDATE_SECTION,
				result: {
					currentSection: 2,
					showCurrentSectionErrors: true,
					userClickedNextButton: true
				}
			});
		} else {
			dispatch(QuoteRequested());

			getBestQuoteService(getBestQuoteAdapter(getState().App))
				.then(result => {
					dispatch(QuoteResponseSuccess(result));

					SubmitForm(dispatch, getState().App);
				})
				.catch(error => {
					dispatch(QuoteResponseFailure(error));
					SubmitForm(dispatch, getState().App);
				});

		}
	};
};

/**
 * @function SubmitForm
 * @param dispatch
 * @param appState
 * @description
 * Handles the submission of the form after quote retrieval was attempted
 */
const SubmitForm = (dispatch, appState) => {
	// Let the store know the user clicked a button!
	dispatch(formSubmitted());

	const formattedEntry = entryAdapter(appState);

	// Evaluate the response and call the appropriate action to be handled!
	formSubmissionService(formattedEntry).then(result => {

		// If the response doesn't return n for Not accepted; then we call it successful
		const wasSuccessful = result.reply.status.accepted !== 'n';

		(wasSuccessful) ? dispatch(formSubmissionSuccess(result))
			: dispatch(formSubmissionFailure(result));
	}).catch(error => {
		dispatch(formSubmissionFailure(error));
	});
};

const QuoteRequested = () => {
	return {
		type: Types.QUOTE_REQUESTED
	};
};

const QuoteResponseSuccess = (result) => {
	return {
		type: Types.QUOTE_RESPONSE_SUCCESS,
		result
	};
};

const QuoteResponseFailure = (error) => {
	return {
		type: Types.QUOTE_RESPONSE_FAILURE,
		error
	};
};

const formSubmitted = () => {
	return {type: Types.FORM_SUBMITTED};
};

const formSubmissionSuccess = (result) => {
	return {
		type: Types.FORM_SUBMISSION_SUCCESS,
		result
	};
};

const formSubmissionFailure = (error) => {
	return {
		type: Types.FORM_SUBMISSION_FAILURE,
		error
	};
};

export default FormSubmitted;

