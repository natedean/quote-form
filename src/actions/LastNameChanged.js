import Types from './AppTypes';

/**
 * @function LastNameChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the Last Name Input
 * */
function LastNameChanged(event) {
    return {
        type: Types.INPUTS.LASTNAME_CHANGED,
        event
    };
}

export default LastNameChanged;