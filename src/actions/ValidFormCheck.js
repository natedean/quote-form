import Types from './AppTypes';

export default function ValidFormCheck(currentFormEntry) {
    return {
        type: Types.VALID_FORM_CHECK,
        currentFormEntry
    };
}