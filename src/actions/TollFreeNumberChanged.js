import Types from './AppTypes';

/**
 * @function TollFreeNumberChanged
 * @params {number} tollFreeNumber - Toll free number to show the user
 * @description
 *   This function dispatches the TOLL_FREE_NUMBER_CHANGED ACTION
 * */
const TollFreeNumberChanged = (tollFreeNumber) => {
	return {
		type: Types.TOLL_FREE_NUMBER_CHANGED,
		tollFreeNumber
	};
};

export default TollFreeNumberChanged;
