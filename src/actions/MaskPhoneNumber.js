import AppTypes from './AppTypes';
import maskPhoneNumber from '../services/maskPhoneNumber';

const MaskPhoneNumber = (phoneNumber) => {
    phoneNumber = maskPhoneNumber(phoneNumber);

    return {
        type: AppTypes.MASK_PHONE_NUMBER,
        phoneNumber
    };
};

export default MaskPhoneNumber;