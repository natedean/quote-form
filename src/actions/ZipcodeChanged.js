import Types from './AppTypes';

/**
 * @function ZipcodeChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the Zipcode Input
 * */
const ZipcodeChanged = (event) => {
	return {
			type: Types.INPUTS.ZIPCODE_CHANGED,
			event
		};
};

export default ZipcodeChanged;
