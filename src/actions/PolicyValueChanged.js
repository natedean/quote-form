import Types from './AppTypes';

/**
 * @function TermLengthChanged
 * @param {number} policyValue
 * @description
 *   This function dispatches the POLICY_VALUE_CHANGED ACTION.
 *   SINCE the PolicyValue input is one targeted for refreshing the displayed Quote,
 *   this function ALSO calls the Quote Dispatcher to hand off all Quote API related logic.
 * */
const PolicyValueChanged = (policyValue) => {
	return (dispatch, getState) => {
		dispatch({type: Types.POLICY_VALUE_CHANGED, policyValue});
	};
};

export default PolicyValueChanged;
