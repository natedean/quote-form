import Types from './AppTypes';

/**
 * @function HasCurrentPolicyChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This lass handles changes to the HasCurrentPolicy Input
 * */
const HasCurrentPolicyChanged = (event) => {
	return {
			type: Types.INPUTS.HAS_CURRENT_POLICY_CHANGED,
			event
		};
};

export default HasCurrentPolicyChanged;
