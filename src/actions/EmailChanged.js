import Types from './AppTypes';

/**
 * @function EmailChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the Email Input
 * */
function EmailChanged(event) {
    return {
        type: Types.INPUTS.EMAIL_CHANGED,
        event
    };
}

export default EmailChanged;