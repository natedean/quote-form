import Types from './AppTypes';

/**
 * @const SliderDragStop
 * @type function
 * @description
 * This function dispatches the SLIDER_STOPPED ACTION.
 * We created a separate action from the onChange action for the slider so that
 * quoteDispatcher does not get called until slider button is released.
 */
const SliderDragStopped = () => {
	return {
			type: Types.SLIDER_DRAG_STOPPED,
		};
};

export default SliderDragStopped;
