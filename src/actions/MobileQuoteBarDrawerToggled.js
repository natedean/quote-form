import AppTypes from './AppTypes';

const MobileQuoteBarDrawerToggled = () => {

	return {
		type: AppTypes.MOBILE_QUOTE_BAR_DRAWER_TOGGLED
	};
};

export default MobileQuoteBarDrawerToggled;
