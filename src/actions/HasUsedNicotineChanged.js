import Types from './AppTypes';

/**
 * @function HasUsedNicotineChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the HasUsedNicotine Input
 * */
const HasUsedNicotineChanged = (event) => {
	return {
			type: Types.INPUTS.HAS_USED_NICOTINE_CHANGED,
			event
		};
};

export default HasUsedNicotineChanged;
