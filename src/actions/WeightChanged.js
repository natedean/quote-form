import Types from './AppTypes';

/**
 * @function WeightChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This function dispatches the WEIGHT_CHANGED action
 * */
const WeightChanged = (event) => {
	return {
			type: Types.INPUTS.WEIGHT_CHANGED,
			event
		};
};

export default WeightChanged;
