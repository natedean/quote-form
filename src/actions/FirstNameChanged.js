import Types from './AppTypes';

/**
 * @function FirstNameChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the First Name Input
 * */
function FirstNameChanged(event) {
    return {
        type: Types.INPUTS.FIRSTNAME_CHANGED,
        event
    };
}

export default FirstNameChanged;