import Types from './AppTypes';
import progressService from '../services/userProgressService';

/**
 * @const UpdateSection
 * @type function
 * @param {number} section
 * @description
 * This action dispatches the current section
 */
const UpdateSection = (section) => {
	return (dispatch, getState) => {
		const App = getState().App;
		const {currentSection, currentFormEntry, incompleteFields} = App;
		const sections = ['basic', 'health', 'contact'];

		if (section >= sections.length || section < currentSection) {
			return dispatch(_updateSection(section, false));
		} else {
			const progress = progressService(sections[currentSection], incompleteFields, currentFormEntry);
			const showCurrentSectionErrors = (Object.keys(progress.incompleteFields).length === 0) ? false : true;
			const sectionToShow = showCurrentSectionErrors ? currentSection : section;

			dispatch(_updateSection(sectionToShow, showCurrentSectionErrors));
		}
	};
};

const _updateSection = (currentSection, showCurrentSectionErrors) => {
	return {
		type: Types.UPDATE_SECTION,
		result: {
			currentSection,
			showCurrentSectionErrors,
			userClickedNextButton: showCurrentSectionErrors
		}
	};
};

export default UpdateSection;
