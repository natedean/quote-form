import Types from './AppTypes';

/**
 * @function SourceChanged
 * @params {object} source - a source from the query string
 * @description
 *   This function dispatches the SOURCE_CHANGED ACTION
 * */
const SourceChanged = (source) => {
	return {
		type: Types.SOURCE_CHANGED,
		source
	};
};

export default SourceChanged;
