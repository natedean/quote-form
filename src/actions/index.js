import PolicyValueChanged from './PolicyValueChanged';
import TermLengthChanged from './TermLengthChanged';
import FormSubmitted from './FormSubmitted';
import ValidFormCheck from './ValidFormCheck';
import MaskPhoneNumber from './MaskPhoneNumber';
import ZipcodeChanged from './ZipcodeChanged';
import FirstNameChanged from './FirstNameChanged';
import LastNameChanged from './LastNameChanged';
import BirthdateChanged from './BirthdateChanged';
import GenderChanged from './GenderChanged';
import PhoneNumberChanged from './PhoneNumberChanged';
import EmailChanged from './EmailChanged';
import HealthRatingChanged from './HealthRatingChanged';
import HasUsedNicotineChanged from './HasUsedNicotineChanged';
import HasCurrentPolicyChanged from './HasCurrentPolicyChanged';
import WeightChanged from './WeightChanged';
import HeightChanged from './HeightChanged';
import MobileQuoteBarDrawerToggled from './MobileQuoteBarDrawerToggled';
import UpdateSection from './UpdateSection';
import SliderDragStopped from './SliderDragStopped';
import CidChanged from './CidChanged';
import SourceChanged from './SourceChanged';
import TollFreeNumberChanged from './TollFreeNumberChanged';
import TrustPilotCustomersChanged from './TrustPilotCustomersChanged';
import ParticipantChanged from './ParticipantChanged';

const Actions = {
	PolicyValueChanged,
	TermLengthChanged,
	FormSubmitted,
	ValidFormCheck,
	MaskPhoneNumber,
	ZipcodeChanged,
	FirstNameChanged,
	LastNameChanged,
	BirthdateChanged,
	GenderChanged,
	PhoneNumberChanged,
	EmailChanged,
	HealthRatingChanged,
	HasUsedNicotineChanged,
	HasCurrentPolicyChanged,
	WeightChanged,
	HeightChanged,
	MobileQuoteBarDrawerToggled,
	UpdateSection,
	SliderDragStopped,
	CidChanged,
	SourceChanged,
	TollFreeNumberChanged,
	TrustPilotCustomersChanged,
	ParticipantChanged
};

export default Actions;
