import Types from './AppTypes';

/**
 * @function CidChanged
 * @params {object} cid - a cid from the query string
 * @description
 *   This function dispatches the CID_CHANGED ACTION
 * */
const CidChanged = (cid) => {
	return {
		type: Types.CID_CHANGED,
		cid
	};
};

export default CidChanged;
