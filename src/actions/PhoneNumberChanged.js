import Types from './AppTypes';

/**
 * @function PhoneNumberChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This lassie handles changes to the Phone Number Input.
 * */
const PhoneNumberChanged = (event) => {
	return {
		type: Types.INPUTS.PHONE_NUMBER_CHANGED,
		event
	};
};

export default PhoneNumberChanged;
