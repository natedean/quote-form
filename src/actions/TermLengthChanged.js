import Types from './AppTypes';

/**
 * @function TermLengthChanged
 * @param {number} termLength
 * @description
 *   This function dispatches the TERM_LENGTH_CHANGED ACTION.
 *   SINCE the TermLength input is one targeted for refreshing the displayed Quote,
 *   this little lass ALSO calls the Quote Dispatcher to hand off all Quote API related logic.
 * */
const TermLengthChanged = (termLength) => {
	return (dispatch, getState) => {

		dispatch({
			type: Types.TERM_LENGTH_CHANGED,
			termLength
		});
	};
};

export default TermLengthChanged;
