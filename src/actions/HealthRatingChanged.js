import Types from './AppTypes';

/**
 * @function HealthRatingChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the HealthRating Input
 *   SINCE the PolicyValue input is one targeted for refreshing the displayed Quote,
 *   this function ALSO calls the Quote Dispatcher to hand off all Quote API related logic.
 * */
const HealthRatingChanged = (event) => {
	return (dispatch, getState) => {
		dispatch({
			type: Types.INPUTS.HEALTH_RATING_CHANGED,
			event
		});
	};
};

export default HealthRatingChanged;
