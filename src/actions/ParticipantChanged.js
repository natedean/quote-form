import Types from './AppTypes';

/**
 * @function ParticipantChanged
 * @params {object} participant - a participant from the query string
 * @description
 *   This function dispatches the PARTICIPANT_CHANGED ACTION
 * */
const ParticipantChanged = (participant) => {
	return {
		type: Types.PARTICIPANT_CHANGED,
		participant
	};
};

export default ParticipantChanged;
