import Types from './AppTypes';

/**
 * @function TrustPilotCustomersChanged
 * @params {object} trustPilotCustomers - how many customers to display from the query string
 * @description
 *   This function dispatches the TRUST_PILOT_CUSTOMERS_CHANGED ACTION
 * */
const TrustPilotCustomersChanged = (trustPilotCustomers) => {
	return {
		type: Types.TRUST_PILOT_CUSTOMERS_CHANGED,
		trustPilotCustomers
	};
};

export default TrustPilotCustomersChanged;
