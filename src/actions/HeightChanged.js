import Types from './AppTypes';

/**
 * @function HeightChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This fella handles changes to the Height Input
 * */
const HeightChanged = (event) => {
	return {
			type: Types.INPUTS.HEIGHT_CHANGED,
			event
		};
};

export default HeightChanged;
