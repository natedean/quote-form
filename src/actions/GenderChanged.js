import Types from './AppTypes';

/**
 * @function GenderChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This action handles changes to the Gender Input
 * */
const GenderChanged = (event) => {
	return {
			type: Types.INPUTS.GENDER_CHANGED,
			event
		};
};

export default GenderChanged;
