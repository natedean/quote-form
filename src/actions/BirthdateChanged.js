import Types from './AppTypes';

/**
 * @function BirthdateChanged
 * @params {object} event - an onChange event from the user
 * @description
 *   This miss dispatches the BIRTHDATE_CHANGED ACTION
 * */
const BirthdateChanged = (event) => {
	return {
			type: Types.INPUTS.BIRTHDATE_CHANGED,
			event
		};
};

export default BirthdateChanged;
