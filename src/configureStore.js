import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import thirdPartyTracking from './middleware/thirdPartyTracking';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
const createLogger = require('redux-logger');
const loggerMiddleware = createLogger();

export default function configureStore(initialState) {

	// Be sure to ONLY add this middleware in development!
	const middleware = process.env.NODE_ENV !== 'production' ?
		[thunk, loggerMiddleware] :
		[thunk, thirdPartyTracking];

	// Be sure to ONLY add this composeWithDevTools in development!
	const composeEnhancers = process.env.NODE_ENV !== 'production' ?
		composeWithDevTools(applyMiddleware(...middleware)) :
		applyMiddleware(...middleware);

	return createStore(
		rootReducer,
		initialState,
		composeEnhancers
	);
}
