const home = (process.env.NODE_ENV !== 'production') ?
	'https://webchat.selectquote.com/life' :
	'https://www.selectquote.com/life';

/**
 * @constant {object} links
 * @description
 * Object for storing list of strings used in desktop/mobile footer, as well as the mobile navigation
 */
const links = {
	home,
	footer: {
		lifeBasics: {
			desktopText: 'What is Term Life Insurance?',
			mobileText: 'Life Insurance Basics',
			textLink: `${home}/learning/what-is-term-life-insurance`
		},
		howMuch: {
			desktopText: 'How much do I need?',
			mobileText: 'How much?',
			textLink: `${home}/calculator/how-much-term-life-insurance-do-i-need`
		},
		howDoesItWork: {
			desktopText: 'How does it work?',
			mobileText: 'How It Works',
			textLink: `${home}/learning/how-it-works`
		},
		sqDifference: {
			desktopText: 'The SelectQuote Difference',
			mobileText: 'The SQ Difference',
			textLink: `${home}/learning/selectquote-difference`
		},
		blog: {
			desktopText: 'Our Blog',
			mobileText: 'Our Blog',
			textLink: 'https://www.selectquote.com/blog'
		},
		otherInsurance: {
			desktopText: 'Auto, Home & Senior Insurance',
			mobileText: 'Auto, Home, Senior',
			textLink: `${home}/auto-home-senior`
		},
		contactUs: {
			desktopText: 'Contact',
			mobileText: 'Contact Us',
			textLink: `${home}/contact`
		}
	},
	social: {
		facebook: 'https://www.facebook.com/SelectQuoteInsuranceServices',
		twitter: 'https://twitter.com/selectquoteins',
		linkedIn: 'https://www.linkedin.com/company/selectquote-insurance-services',
		youtube: 'https://www.youtube.com/user/selectquote'
	},
	legal: {
		privacyPolicy: `${home}/privacy`,
		legalInfo: `${home}/legal`,
		carriers: `${home}/about-selectquote/our-carriers`,
		disclaimers: `${home}/learning/disclaimer_list`
	}

};

export default links;
