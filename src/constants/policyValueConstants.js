/**
 * @const {object} policyValueConstants
 * @property {number} min - The minimum allowable policy value
 * @property {number} max - The maximum allowable policy value
 * @property {number} multipleOf - The policy value must be a multiple of this number
 * @property {number} conversionFactor - The policy value conversion factor to convert to/from internal format
 * @description
 * Contains constants for checking the policy value.
 */
const policyValueConstants = {
	min: 1e5,
	max: 1e7,
	multipleOf: 50000,
	conversionFactor: 1e7
};

export default policyValueConstants;
