/**
 * @constant constants
 * @type {object}
 * @description
 * Object for storing list of constants used across SQLife site
 */
const constants = {
	basicHeader: 'Complete this form to see how low your rates could be.',
	healthHeader: 'Keep going to see how low your rates could be.',
	subHeaderText: 'Then speak with one or our life insurance specialists to get an exact rate for your specific needs.',
	contactHeader: 'Almost Done.',
	contactSubHeader: 'Just a few more questions to see how low your rate could be.',
	failureHeader: 'Your situation is unique.',
	failureSubHeader: 'Based on the information provided we cannot display an online estimate.'
};

export default constants;
