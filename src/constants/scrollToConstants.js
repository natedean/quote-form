/**
 * @constant constants
 * @type {object}
 * @description
 * Constants for the focusInputField service
 */
const constants = {
	windowSizeBreakpoint: 768,
	inputs: {
		zipcode: {
			offsetReduction: 250
		},
		birthdateInput: {
			offsetReduction: 150
		},
		bodyInput: {
			offsetReduction: 150
		},
		firstName: {
			offsetReduction: 220
		},
		lastName: {
			offsetReduction: 220
		},
		email: {
			offsetReduction: 220
		},
		phoneNumber: {
			offsetReduction: 220
		},
		healthRating: {
			offsetReduction: -100
		},
		hasCurrentPolicy: {
			offsetReduction: 220
		},
		hasUsedNicotine: {
			offsetReduction: 220
		},
		gender: {
			offsetReduction: 220
		}
	}
};

export default constants;
