/**
 * @const {object} termLengthConstants
 * @property {number} min - The minimum allowable policy value
 * @property {number} max - The maximum allowable policy value
 * @property {number} multipleOf - The policy value must be a multiple of this number
 * @property {number} conversionFactor - The policy value conversion factor to convert to/from internal format
 * @description
 * Contains constants for checking the term length.
 */
const termLengthConstants = {
	min: 10,
	max: 30,
	multipleOf: 5,
	conversionFactor: 100
};

export default termLengthConstants;
