import Months from './Months';
let collection = Object.keys(Months);
let daysInMonth = [];

function determineDaysInMonth(monthIndex) {
    if (Array.isArray(collection) && collection[monthIndex]) {

        /** If this month has 31 days*/
        if (monthIndex === 0 || monthIndex === 2 || monthIndex === 4
            || monthIndex === 6 || monthIndex === 7 || monthIndex === 9 || monthIndex === 11) {
            daysInMonth.push(31);

            /** If this month is February*/
        } else if (monthIndex === 1) {
            daysInMonth.push(29);
        } else {
            daysInMonth.push(30);
        }
    }
}

collection.forEach((item, index) => {
    determineDaysInMonth(index);
});

export default daysInMonth;