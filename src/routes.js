import React from 'react';
import {Route} from 'react-router';

import ShellContainer from './containers/ShellContainer';

export default (
    <Route path="/" component={ShellContainer} />
);