/* eslint-disable */
import greenStar from './green_star.png';
import trustPilot from './trustpilot.png';
import facebookIcon from './Facebook.png';
import linkedInIcon from './Linkedin.png';
import twitterIcon from './Twitter.png';
import youtubeIcon from  './Youtube.png';

/* eslint-enable */

export const socialIcons = {
    facebookIcon,
    linkedInIcon,
    twitterIcon,
    youtubeIcon
};

export const trustPilotIcons = {
    greenStar,
    trustPilot
};

