import Types from '../actions/AppTypes';
import initialState from './initialAppState';
import updateState from '../services/updateState';

function appReducer(state = initialState, action) {
	if (!action) {
		throw new Error('No action? Is that like an empty dispatch()?');
	}

	if (action.type in Types.INPUTS) {
		return updateState(action.event, state);
	}

	switch (action.type) {
		case Types.POLICY_VALUE_CHANGED: {
			let currentFormEntry = Object.assign({}, state.currentFormEntry);

			currentFormEntry.policyValue = action.policyValue;
			return Object.assign({}, state, {currentFormEntry});
		}
		case Types.TERM_LENGTH_CHANGED: {
			let currentFormEntry = Object.assign({}, state.currentFormEntry);

			currentFormEntry.termLength = action.termLength;
			return Object.assign({}, state, {currentFormEntry});
		}
		case Types.FORM_SUBMITTED: {
			return Object.assign({}, state, {formIsSubmitting: true, submissionErrorMessage: ''});
		}
		case Types.FORM_SUBMISSION_SUCCESS: {
			const redirectLink = action.result.reply.status.url;

			return Object.assign({}, state, {formIsSubmitting: false, redirectLink});
		}
		case Types.FORM_SUBMISSION_FAILURE: {
			const submissionErrorMessage = (action.error) ? action.error.reply.status.detail : '';

			return Object.assign({}, state, {formIsSubmitting: false, submissionErrorMessage});
		}

		case Types.QUOTE_REQUESTED: {
			return Object.assign({}, state, {quoteIsBeingRequested: true});
		}

		case Types.QUOTE_RESPONSE_SUCCESS: {
			const {
				MonthlyPremium: quoteAmountMonth, AnnualPremium:quoteAmountYear, CarrierID:carrierID,
				Duration:selectedDuration, InsCoName: insCoName, ApplicationName, UWClass
			} = action.result;

			//TODO Follow up on trailing whitespace by getting it removed from the response or trimming it ourselves
			const policyName = `${insCoName} ${ApplicationName} ${UWClass}`;

			return Object.assign({}, state, {
				quoteIsBeingRequested: false,
				quoteAmountMonth,
				quoteAmountYear,
				selectedDuration,
				insCoName,
				policyName,
				carrierID
			});
		}

		case Types.QUOTE_RESPONSE_FAILURE: {
			return Object.assign({}, state, {
				quoteIsBeingRequested: false,
				quoteAmountMonth: '',
				quoteAmountYear: '',
				insCoName: '',
				policyName: '',
				carrierID: ''
			});
		}

		case Types.VALID_FORM_CHECK: {
			return Object.assign({}, state, {currentFormEntry: action.currentFormEntry});
		}
		case Types.MASK_PHONE_NUMBER: {
			return Object.assign({}, state, {phoneNumber: action.phoneNumber});
		}
		case Types.MOBILE_QUOTE_BAR_DRAWER_TOGGLED: {
			return Object.assign({}, state, {drawerIsOpen: !state.drawerIsOpen});
		}
		case Types.UPDATE_SECTION: {
			const {currentSection, showCurrentSectionErrors, userClickedNextButton} = action.result;
			return Object.assign({}, state, {currentSection, showCurrentSectionErrors, userClickedNextButton});
		}
		case Types.SOURCE_CHANGED: {
			return Object.assign({}, state, {source: action.source});
		}
		case Types.CID_CHANGED: {
			return Object.assign({}, state, {cid: action.cid});
		}
		case Types.TOLL_FREE_NUMBER_CHANGED: {
			return Object.assign({}, state, {tollFreeNumber: action.tollFreeNumber});
		}
		case Types.TRUST_PILOT_CUSTOMERS_CHANGED: {
			return Object.assign({}, state, {trustPilotCustomers: action.trustPilotCustomers});
		}
		case Types.PARTICIPANT_CHANGED: {
			return Object.assign({}, state, {participant: action.participant});
		}
		default: {
			return state;
		}
	}
}

export default appReducer;
