import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import AppReducer from './AppReducer';

const reducers = combineReducers({
    routing: routerReducer,
    App: AppReducer
});

export default reducers;