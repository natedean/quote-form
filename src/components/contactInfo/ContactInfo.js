import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import FirstNameInput from './FirstNameInput';
import LastNameInput from './LastNameInput';
import EmailInput from './EmailInput';
import PhoneInput from './PhoneInput';

/**
 * @class ContactInfo
 * @param {object} props
 * @description
 * Used to display the contact information form that allows a potential
 * customer to input their contact details so that a sales agent may follow
 * up.
 */
const ContactInfo = (props) => {
	const {
		headerText, subHeaderText, emailErrorText, drawerIsOpen,
		phoneErrorText, currentFormEntry, actions, showCurrentSectionErrors
	} = props;

	return (
		<div className={`contactInfo contentContainer${drawerIsOpen ? ' contentContainer--mobileDrawerOpen' : ''}`}>
			<Row className="contactInfo__header">
				<Col xs="12">
					<h2>{headerText}</h2>
					<div className="h5">{subHeaderText}</div>
				</Col>
			</Row>
			<Row>
				<Col xs="12" md="6">
					<FirstNameInput
						firstName={currentFormEntry['firstName']}
						updateState={(event) => actions.firstNameChanged(event)}
						errorText={'Please enter your First Name'}
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['firstName'])
						|| (showCurrentSectionErrors && currentFormEntry['firstName'].length < 2)}
					/>
					<LastNameInput
						lastName={currentFormEntry['lastName']}
						updateState={(event) => actions.lastNameChanged(event)}
						errorText={'Please enter your Last Name'}
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['lastName'])
						|| (showCurrentSectionErrors && currentFormEntry['lastName'].length < 2)}
					/>
				</Col>
				<Col xs="12" md="6">
					<EmailInput
						email={currentFormEntry['email']}
						errorText={'Please enter a valid Email'}
						updateState={(event) => actions.emailChanged(event)}
						titleText="Email"
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['email'])
						|| (showCurrentSectionErrors && Boolean(emailErrorText))}
					/>
					<PhoneInput
						phoneNumber={currentFormEntry['phoneNumber']}
						errorText={'Please enter a valid Phone Number'}
						updateState={(event) => actions.phoneNumberChanged(event)}
						titleText="Phone"
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['phoneNumber'])
						|| (showCurrentSectionErrors && Boolean(phoneErrorText))}
					/>
				</Col>
			</Row>
		</div>
	);
};

ContactInfo.propTypes = {
	headerText: React.PropTypes.string,
	subHeaderText: React.PropTypes.string,
	emailErrorText: React.PropTypes.string,
	phoneErrorText: React.PropTypes.string,
	firstNameErrorText: React.PropTypes.string,
	lastNameErrorText: React.PropTypes.string,
	actions: React.PropTypes.object,
	currentFormEntry: React.PropTypes.object,
	showCurrentSectionErrors: React.PropTypes.bool,
	drawerIsOpen: React.PropTypes.bool
};

export default ContactInfo;
