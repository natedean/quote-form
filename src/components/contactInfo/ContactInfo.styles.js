let scssVars = require('../../styles/scss/base/variables/variables.json');

const styles = {
	textField: {
		names: {
			backgroundColor: scssVars.colors.whiteGray,
			borderRadius: '8px',
			marginTop: '1em',
			paddingLeft: '15px',
			fontSize: '1.3em',
			width: '75%'
		},
		email: {
			backgroundColor: scssVars.colors.whiteGray,
			borderRadius: '8px',
			marginTop: '1em',
			paddingLeft: '15px',
			fontSize: '1.3em',
			width: '100%'
		},
		phoneNumber: {
			backgroundColor: scssVars.colors.whiteGray,
			borderRadius: '8px',
			marginTop: '1em',
			paddingLeft: '15px',
			fontSize: '1.3em',
		}
	},
	titleSubLinks: {
		textAlign: 'center',
		paddingTop: '20px',
		paddingBottom: '80px'
	},
	rightColumn: {
		paddingLeft: '130px'
	},
	errorMessageContainer: {
		minHeight: '70px',
		maxHeight: '70px',
		marginLeft: '-20px'
	},
	errorMessage: {
		marginLeft: '-15px',
		paddingTop: '8px',
		fontSize: '.6em',
		color: scssVars.colors.pink,
		lineHeight: '1em',
		width: '350px'
	},
	input: {
		color: 'black'
	}
};

export default styles;
