import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import styles from './ContactInfo.styles';
import ErrorText from '../common/ErrorText';
import Title from '../common/Title';
import scrollToInput from '../../services/scrollToInput';
import CustomPhoneInput from '../common/CustomPhoneInput';

/**
 * @function PhoneInput
 * @param {object} props
 * @description
 *   This component is responsible for handling phone number input
 * */
const PhoneInput = (props) => {
	const {titleText, phoneNumber, updateState, errorText, propIsInvalid} = props;

	return (
		<Row className="phoneInput">
			<Col xs="2">
				<Title className="emailInput__label" titleText={titleText} />
			</Col>
			<Col xs="10" style={styles.errorMessageContainer}>
				<CustomPhoneInput
					phoneNumber={phoneNumber}
					updateState={updateState}
					scrollToPhoneInput={scrollToInput}
				/>
				<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
			</Col>
		</Row>
	);
};

PhoneInput.propTypes = {
	phoneNumber: React.PropTypes.string,
	updateState: React.PropTypes.func,
	titleText: React.PropTypes.string,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default PhoneInput;
