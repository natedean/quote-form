import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import TextField from 'material-ui/TextField';
import styles from './ContactInfo.styles';
import ErrorText from '../common/ErrorText';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function LastNameInput
 * @param {object} props
 * @description
 *  This component handles the Last Name Input
 * */
const LastNameInput = (props) => {
	const {lastName, updateState, errorText, propIsInvalid} = props;

	return (
		<Row className="lastNameInput">
			<Col xs="12" style={styles.errorMessageContainer}>
				<TextField
					value={lastName || ''}
					onChange={(event) => updateState(event)}
					name="lastName"
					underlineShow={false}
					hintText="Last Name"
					inputStyle={styles.input}
					style={styles.textField.names}
					onFocus={scrollToInput}
				/>
			</Col>
			<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
		</Row>
	);
};

LastNameInput.propTypes = {
	lastName: React.PropTypes.string,
	updateState: React.PropTypes.func,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default LastNameInput;
