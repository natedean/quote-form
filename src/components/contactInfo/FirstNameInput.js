import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import TextField from 'material-ui/TextField';
import styles from './ContactInfo.styles';
import ErrorText from '../common/ErrorText';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function FirstNameInput
 * @param {object} props
 * @description
 *   This component handles the First Name Input
 * */
const FirstNameInput = (props) => {
	const {firstName, updateState, errorText, propIsInvalid} = props;

	return (
		<Row className="firstNameInput">
			<Col xs="12" style={styles.errorMessageContainer}>
				<TextField
					value={firstName || ''}
					onChange={(event) => updateState(event)}
					name="firstName"
					underlineShow={false}
					hintText="First Name"
					inputStyle={styles.input}
					style={styles.textField.names}
					onFocus={scrollToInput}
				/>
			</Col>
			<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
		</Row>
	);
};

FirstNameInput.propTypes = {
	firstName: React.PropTypes.string,
	updateState: React.PropTypes.func,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default FirstNameInput;
