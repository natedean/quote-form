import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import TextField from 'material-ui/TextField';
import styles from './ContactInfo.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function EmailInput
 * @param {object} props
 * @description
 *   This component handles email input from the user.
 * */
const EmailInput = (props) => {
	const {titleText, email, updateState, errorText, propIsInvalid} = props;

	return (
		<Row className="emailInput">
			<Col xs="2">
				<Title className="emailInput__label" titleText={titleText} />
			</Col>
			<Col xs="10" style={styles.errorMessageContainer}>
				<TextField
					value={email || ''}
					onChange={(event) => updateState(event)}
					name="email"
					underlineShow={false}
					hintText="name@email.com"
					inputStyle={styles.input}
					style={styles.textField.email}
					type="email"
					onFocus={scrollToInput}
				/>
				<ErrorText errorText={errorText} showErrorText={propIsInvalid}/>
			</Col>
		</Row>
	);
};

EmailInput.propTypes = {
	email: React.PropTypes.string,
	updateState: React.PropTypes.func,
	titleText: React.PropTypes.string,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default EmailInput;
