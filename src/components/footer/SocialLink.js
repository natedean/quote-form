import React from 'react';
import SvgIcon from '../common/SvgIcon';

/**
 * @function SocialLink
 * @description
 * Organizes SQ Social Media Links including facebook, twitter
 * linkedin and youtube
 */
const SocialLink = (props) => {
	const {drawPaths, drawPolygons, href} = props;

	return (
		<a className="socialIcons__iconButton"
			href={href}>
			<SvgIcon
				drawPaths={drawPaths}
				drawPolygons={drawPolygons}
				svgWrapperClass="socialIcons__svgWrapper"
				svgIconClass="socialIcons__svgIcon"
				viewBox="0 0 36 36"
			/>
		</a>
	);
};

export default SocialLink;

SocialLink.propTypes = {
	drawPaths: React.PropTypes.array,
	drawPolygons: React.PropTypes.array,
	href: React.PropTypes.string
};
