
const styles = {
    footerInfo: {
		wrapper: {
			display: 'flex'
		},
		icon: {
			float: 'left',
			height: '30px',
			width: '30px',
			color: '#00a5bc'
		}
	}
};

export default styles;
