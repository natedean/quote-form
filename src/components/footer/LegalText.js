import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import links from '../../constants/textLinks';
import transamericaCarrierIDTable from '../../constants/transamericaCarrierIDTable';

/**
 * @function LegalText
 * @description
 * Displays required legal information for the page
 */
const LegalText = (props) => {
	const {
		policyName, carrierID
	} = props;

	return (
		<Row className="legalText">
			<Col xs="12" className="legalText__linkWrapper">
				<div>
					<a href={links.legal.privacyPolicy} className="color--slate">Privacy Policy</a> |&nbsp;
					<a href={links.legal.legalInfo} className="color--slate">Legal Information</a>
				</div>
			</Col>
			<Col xs="12" className="legalText__blockTextWrapper">
				<div>
					<p>
						License name varies by state: SelectQuote Insurance Services, SelectQuote Insurance Agency,
						&copy; 2016 SelectQuote Insurance Services. All rights reserved.
					</p>
					<p>
						SelectQuote is not licensed to do business in South Dakota.
					</p>
					{policyName &&
						<p>
							{policyName}
						</p>
					}
					{transamericaCarrierIDTable[carrierID] &&
						<p>
							Term Life Insurance products issued by Transamerica Life Insurance Company,
							Cedar Rapids, IA or in New York, Transamerica Financial Life Insurance Company,
							Harrison, NY. Premiums increase annually beginning in year 11 for the 10-year policy,
							in year 16 for the 15-year policy, in year 21 for the 20-year policy, in year 26 for
							the 25 year policy, and in year 31 for the 30-year policy. Policy form and number may
							vary, and these policies may not be available in all jurisdictions. Insurance
							eligibility and premiums are subject to underwriting. In most states, in the event of
							suicide during the first two policy years, death benefits are limited only to the
							return of premiums paid.
						</p>
					}
					<p>
						For detailed information regarding the companies above, please&nbsp;
						<a href={links.legal.carriers} className="color--slate">click here</a>.
					</p>
					<p>
						For full policy information, please&nbsp;
						<a href={links.legal.disclaimers} className="color--slate">click here</a>.
					</p>
				</div>
			</Col>
		</Row>
	);
};

LegalText.propTypes = {
	policyName: React.PropTypes.string,
	carrierID: React.PropTypes.string
};

export default LegalText;
