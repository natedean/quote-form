import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import links from '../../constants/textLinks';
import svgDraw from '../../constants/svgDraw';
import SocialLink from './SocialLink';

/**
 * @function SocialLinks
 * @description
 * Organizes SQ Social Media Links including facebook, twitter
 * linkedin and youtube
 */
const SocialLinks = () => {
	return (
		<Row>
			<Col sm="12" className="socialIcons__wrapper">
				<SocialLink
					drawPaths={svgDraw.socialIcons.facebook.path}
					href={links.social.facebook}
				/>
				<SocialLink
					drawPaths={svgDraw.socialIcons.twitter.path}
					href={links.social.twitter}
				/>
				<SocialLink
					drawPaths={svgDraw.socialIcons.linkedIn.path}
					href={links.social.linkedIn}
				/>
				<SocialLink
					drawPaths={svgDraw.socialIcons.youtube.path}
					drawPolygons={svgDraw.socialIcons.youtube.polygon}
					href={links.social.youtube}
				/>
			</Col>
		</Row>
	);
};

export default SocialLinks;
