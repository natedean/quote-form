import React from 'react';
import InfoLinkWrapper from './InfoLinksWrapper';
import FooterInfo from './FooterInfo';
import SocialLinksWrapper from './SocialLinksWrapper';
import LegalTextContainer from '../../containers/LegalTextContainer';

/**
 * @function Footer
 * @param {string} props.trustPilotCustomers - Current number of TRUSTPILOT customers.
 * @param {string} props.tollFreeNumber - Phone number.
 * @description
 * Top-level component for the footer category of components
 */
const Footer = ({trustPilotCustomers, tollFreeNumber}) => {
	return (
		<div>
			<div className="horizontalRule"/>
			<InfoLinkWrapper />
			<FooterInfo
				trustPilotCustomers={trustPilotCustomers}
				tollFreeNumber={tollFreeNumber}
			/>
			<SocialLinksWrapper />
			<LegalTextContainer />
		</div>
	);
};

Footer.propTypes = {
	trustPilotCustomers: React.PropTypes.string,
	tollFreeNumber: React.PropTypes.string
};

export default Footer;
