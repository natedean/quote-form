import React from 'react';
import links from '../../constants/textLinks';
import InfoLink from './InfoLink';
import svgDraw from '../../constants/svgDraw';

/**
 * @class InfoLinkWrapper
 * @description
 * Flexbox styled wrapper for containing all of the infoLink Components
 */

const InfoLinksWrapper = () => {
	return (
		<div className="infoLinkContainer">
			<div className="infoLinkContainer__mobileTitle">
				More Information:
			</div>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.heartIcon.path}
				infoTextDesktop={links.footer.lifeBasics.desktopText}
				infoTextMobile={links.footer.lifeBasics.mobileText}
				href={links.footer.lifeBasics.textLink}
			/>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.dollarIcon.path}
				infoTextDesktop={links.footer.howMuch.desktopText}
				infoTextMobile={links.footer.howMuch.mobileText}
				href={links.footer.howMuch.textLink}
			/>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.gearIcon.path}
				infoTextDesktop={links.footer.howDoesItWork.desktopText}
				infoTextMobile={links.footer.howDoesItWork.mobileText}
				href={links.footer.howDoesItWork.textLink}
			/>

			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.starIcon.path}
				infoTextDesktop={links.footer.sqDifference.desktopText}
				infoTextMobile={links.footer.sqDifference.mobileText}
				href={links.footer.sqDifference.textLink}
			/>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.shareIcon.path}
				infoTextDesktop={links.footer.blog.desktopText}
				infoTextMobile={links.footer.blog.mobileText}
				href={links.footer.blog.textLink}
			/>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.umbrellaIcon.path}
				infoTextDesktop={links.footer.otherInsurance.desktopText}
				infoTextMobile={links.footer.otherInsurance.mobileText}
				href={links.footer.otherInsurance.textLink}
			/>
			<InfoLink
				svgIcon={svgDraw.infoLinkIcons.avatarIcon.path}
				infoTextDesktop={links.footer.contactUs.desktopText}
				infoTextMobile={links.footer.contactUs.mobileText}
				href={links.footer.contactUs.textLink}

			/>
		</div>
	);
};

export default InfoLinksWrapper;
