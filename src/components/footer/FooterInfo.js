import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import ActionDoneIcon from 'material-ui/svg-icons/action/done';
import {trustPilotIcons} from '../../images';
import styles from './Footer.styles';
import maskPhoneNumber from '../../services/maskPhoneNumber';

/**
 * @function FooterInfo
 * @param {string} props.trustPilotCustomers - Current number of TRUSTPILOT customers.
 * @param {string} props.tollFreeNumber - Phone number.
 * @description
 * This component displays static information about potential health
 * concerns and trust pilot rating
 */
const FooterInfo = ({trustPilotCustomers, tollFreeNumber}) => {
	return (
		<Row className="footerInfoWrapper">
			<Col sm="9" className="healthConcernsColumn">
				<Row className="healthConcernsColumn__row">
					<Col xs="12" md="7">
						<div className="healthConcernsColumn__header">
							Health Concerns? SelectQuote Can Help.
						</div>
						<div className="healthConcernsColumn__blockText" >
							We shop over 70 policies so chances are we can find the right policy and price for your
							situation. Discuss your concerns with a Life Insurance Advisor now.
							<p>{maskPhoneNumber(tollFreeNumber, true)}</p>
						</div>
					</Col>

					<Col xs="12" md="5" className="healthConcernsColumn__checkList">
						<div className="healthConcernsColumn__checkText">
							<ActionDoneIcon style={styles.footerInfo.icon}/>
							Diabetes
						</div>
						<div className="healthConcernsColumn__checkText">
							<ActionDoneIcon style={styles.footerInfo.icon}/>
							Heart Disease
						</div>
						<div className="healthConcernsColumn__checkText">
							<ActionDoneIcon style={styles.footerInfo.icon}/>
							Cancer
						</div>
						<div className="healthConcernsColumn__checkText">
							<ActionDoneIcon style={styles.footerInfo.icon}/>
							Sleep Apnea
						</div>
						<div className="healthConcernsColumn__checkText">
							<ActionDoneIcon style={styles.footerInfo.icon}/>
							Overweight
						</div>
					</Col>
				</Row>
			</Col>
			<Col xs="12" sm="3" className="trustPilotColumn">
				<div className="marginAuto">
					{trustPilotCustomers} customers say we're:
					<p className="trustPilotColumn__rating">
						Great
					</p>
					<div className="trustPilotColumn__iconsWrapper">
						<img src={trustPilotIcons.greenStar} alt="TrustPilot Icon" className="trustPilotColumn__img"/>
						<img src={trustPilotIcons.greenStar} alt="TrustPilot Icon" className="trustPilotColumn__img"/>
						<img src={trustPilotIcons.greenStar} alt="TrustPilot Icon" className="trustPilotColumn__img"/>
						<img src={trustPilotIcons.greenStar} alt="TrustPilot Icon" className="trustPilotColumn__img"/>
					</div>
					<span className="trustPilotColumn__logoText">Powered by</span>
					<span>
						<img src={trustPilotIcons.trustPilot} alt="TRUSTPILOT" className="trustPilotColumn__logo"/>
					</span>
				</div>
			</Col>
		</Row>
	);
};

FooterInfo.propTypes = {
	trustPilotCustomers: React.PropTypes.string,
	tollFreeNumber: React.PropTypes.string
};

export default FooterInfo;
