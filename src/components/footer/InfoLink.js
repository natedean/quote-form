import React from 'react';
import SvgIcon from '../common/SvgIcon';
import svgDraw from '../../constants/svgDraw';
/**
 * @class InfoLink
 * @param {string} props.svgIcon- array of svg draw paths
 * @param {string} props.infoTextDesktop - text to be displayed for desktop display of component
 * @param {string} props.infoTextMobile - text to be displayed for mobile display of component
 * @description
 * Used to display a single info link in the footer of the life page.
 * Because both the text and images change from the desktop to mobile
 * versions of the screen, there are separate objects for both the
 * desktop and mobile
 */

const InfoLink = (props) => {

	const {svgIcon, infoTextDesktop, infoTextMobile, href} = (props);

	return (
		<a href={href} className="infoLink">
			<SvgIcon
				drawPaths={[...svgIcon, ...svgDraw.infoLinkIcons.circleBorder.path]}
				svgWrapperClass="infoLink__svgWrapper"
				svgIconClass="infoLink__svgIcon"
				viewBox="0 0 42 42"
			/>
			<div className="infoLink__messageDesktop">
				{infoTextDesktop}
			</div>
			<div className="infoLink__messageMobile">
				{infoTextMobile}
			</div>
		</a>
	);
};

export default InfoLink;

InfoLink.propTypes = {
	svgIcon: React.PropTypes.array.isRequired,
    infoTextDesktop: React.PropTypes.string.isRequired,
    infoTextMobile: React.PropTypes.string.isRequired,
	href: React.PropTypes.string.isRequired
};
