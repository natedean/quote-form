import React from 'react';
import BasicInfoContainer from '../containers/BasicInfoContainer';
import ContactInfoContainer from '../containers/ContactInfoContainer';
import HealthInfoContainer from '../containers/HealthInfoContainer';
import SubmissionContainer from '../containers/SubmissionContainer';
import scroll from 'react-scroll/lib/mixins/animate-scroll';
/**
 * @class QuoteForm
 * @param {object} props - see propTypes below
 * @description
 *   This component is a wrapper for the Quote form sections
 *   It handles state transition and scrolling to ease the responsibilities of App.js
 */
class QuoteForm extends React.Component {
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps() {
		const currentWindowWidth = window.innerWidth;
		const offsetTop = this.refs.transitionWrapper.offsetTop;
		const windowSizeBreakpoint = 768;
		const offsetReduction = 125;

		currentWindowWidth < windowSizeBreakpoint ? scroll.scrollTo(0) : scroll.scrollTo(offsetTop - offsetReduction);
	}

	render() {
		const {currentSection} = this.props;
		const BasicInfoSectionIndex = 0;
		const HealthInfoSectionIndex = 1;
		const ContactInfoSectionIndex = 2;
		const stepClasses = [];

		if (currentSection === 0) {
			stepClasses.push('transition--current');
			stepClasses.push('transition--right');
			stepClasses.push('transition--right');
		}
		if (currentSection === 1) {
			stepClasses.push('transition--left');
			stepClasses.push('transition--current');
			stepClasses.push('transition--right');
		}
		if (currentSection === 2) {
			stepClasses.push('transition--left');
			stepClasses.push('transition--left');
			stepClasses.push('transition--current');
		}

		return (
			<div>
				<div className="transitionWrapper" ref="transitionWrapper">
					<div className={`transition ${stepClasses[BasicInfoSectionIndex]}`}>
						<BasicInfoContainer />
					</div>
					<div className={`transition ${stepClasses[HealthInfoSectionIndex]}`}>
						<HealthInfoContainer />
					</div>
					<div className={`transition ${stepClasses[ContactInfoSectionIndex]}`}>
						<ContactInfoContainer />
						<SubmissionContainer />
					</div>
				</div>
			</div>
		);
	}
}

QuoteForm.propTypes = {
	currentSection: React.PropTypes.number
};

export default QuoteForm;
