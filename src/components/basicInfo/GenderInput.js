import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import radioStyles from '../common/RadioButton.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import RadioButtonIcons from '../common/RadioButtonIcons';

/**
 * @function GenderInput
 * @param {object} props
 * @description
 *   This Input is responsible for toggling Gender
 * */
const GenderInput = (props) => {
	const {titleText, gender, updateState, disabled, propIsInvalid, errorText} = props;
	const {checkedIcon, uncheckedIcon} = RadioButtonIcons;

	return (
		<div className="genderInput">
			<Row>
				<Col xs="12">
					<Title titleText={titleText}/>
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<RadioButtonGroup
						name="gender"
						className="flex"
						defaultSelected={gender}
						onChange={(event, value) => {
							updateState({target: {name:'gender', value}});
						}}
					>
						<RadioButton
							checkedIcon={checkedIcon}
							uncheckedIcon={uncheckedIcon}
							iconStyle={radioStyles.radioButtonIcon}
							labelStyle={radioStyles.radioButtonLabel}
							style={radioStyles.radioButton}
							disabled={disabled}
							value="male"
							label="Male"
						/>
						<RadioButton
							checkedIcon={checkedIcon}
							uncheckedIcon={uncheckedIcon}
							iconStyle={radioStyles.radioButtonIcon}
							labelStyle={radioStyles.radioButtonLabel}
							style={radioStyles.radioButton}
							disabled={disabled}
							value="female"
							label="Female"
						/>
					</RadioButtonGroup>
				</Col>
				<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
			</Row>
		</div>
	);
};

GenderInput.propTypes = {
	gender: React.PropTypes.string,
	updateState:React.PropTypes.func,
	titleText: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	propIsInvalid: React.PropTypes.bool,
	errorText: React.PropTypes.string
};

export default GenderInput;
