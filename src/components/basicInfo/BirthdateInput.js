import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import CustomSelectField from '../common/CustomSelectField';
import {getBirthdateInputValues} from '../../services/birthdateFormatters';
import styles from './BasicInfo.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function BirthdateInput
 * @param {object} param
 * @description
 *   This Input is responsible for users selecting their Birthdate
 * */
const BirthdateInput = (props) => {

	const {titleText, birthdate, updateState, disabled, propIsInvalid, errorText} = props;
	const {formattedMonths, formattedDays, formattedYears} = getBirthdateInputValues(birthdate);

	return (
		<div className="birthdateInput">
			<Row>
				<Col xs="12">
					<Title titleText={titleText} />
				</Col>
			</Row>
			<Row>
				<Col xs="12" sm="4" md="12" lg="4" style={styles.errorMessageContainer} className="Select-wrapper">
					<CustomSelectField
						disabled={disabled}
						placeholder="Month"
						name="birthMonth"
						value={birthdate && birthdate['birthMonth']}
						collection={formattedMonths}
						updateState={(newValue) => {
							updateState({
								target: {
									name: 'birthdate',
									value: Object.assign({}, birthdate, {birthMonth: newValue.value})
								}
							});
						}}
						onOpen={() => scrollToInput({default: {name: 'birthdateInput'}})}
					/>
				</Col>
				<Col xs="12" sm="4" md="12" lg="4" className="Select-wrapper">
					<CustomSelectField
						disabled={disabled}
						placeholder="Day"
						name="dayInMonth"
						value={birthdate && birthdate['dayInMonth'] ? birthdate['dayInMonth'] : null}
						collection={formattedDays}
						updateState={(newValue) => {
							updateState({
								target: {
									name: 'birthdate',
									value: Object.assign({}, birthdate, {dayInMonth: newValue.value})
								}
							});
						}}
						onOpen={() => scrollToInput({default: {name: 'birthdateInput'}})}
					/>
				</Col>
				<Col xs="12" sm="4" md="12" lg="4" className="Select-wrapper">
					<CustomSelectField
						disabled={disabled}
						placeholder="Year"
						name="birthYear"
						value={birthdate && birthdate['birthYear'] ? birthdate['birthYear'] : null}
						collection={formattedYears.sort((a, b) => b.value - a.value)}
						updateState={(newValue) => {
							updateState({
								target: {
									name: 'birthdate',
									value: Object.assign({}, birthdate, {birthYear: newValue.value})
								}
							});
						}}
						onOpen={() => scrollToInput({default: {name: 'birthdateInput'}})}
					/>
				</Col>
				<ErrorText errorText={errorText} showErrorText={propIsInvalid}/>
			</Row>
		</div>
	);
};

BirthdateInput.propTypes = {
	titleText: React.PropTypes.string,
	birthdate: React.PropTypes.object,
	updateState: React.PropTypes.func,
	disabled: React.PropTypes.bool,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default BirthdateInput;
