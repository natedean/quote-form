import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import TextField from 'material-ui/TextField';
import styles from './BasicInfo.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function ZipcodeInput
 * @param {object} props
 * @description
 *   This component handles the zipcode Input entry!
 * */
const ZipcodeInput = (props) => {
	const {
		titleText, zipcode, updateState, errorText, disabled, propIsInvalid
	} = props;

	return (
		<div className="zipcodeInput">
			<Row>
				<Col xs="12">
					<Title titleText={titleText} />
				</Col>
			</Row>
			<Row>
				<Col xs="12" style={styles.errorMessageContainer}>
					<TextField
						value={zipcode}
						onChange={(event) => updateState(event)}
						name="zipcode"
						underlineShow={false}
						disabled={disabled}
						hintText="- - - - -"
						inputStyle={styles.zipcode.input}
						style={styles.zipcode.textfield}
						onFocus={scrollToInput}
					/>
				</Col>
				<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
			</Row>
		</div>
	);
};

ZipcodeInput.propTypes = {
	zipcode: React.PropTypes.string,
	updateState: React.PropTypes.func,
	titleText: React.PropTypes.string,
	errorText: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	propIsInvalid: React.PropTypes.bool
};

export default ZipcodeInput;
