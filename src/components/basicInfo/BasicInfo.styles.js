let scssVars = require('../../styles/scss/base/variables/variables.json');

const styles = {
	errorMessageContainer: {
		minHeight: '70px',
		maxHeight: '70px'
	},
	errorMessage: {
		marginLeft: '-15px',
		paddingTop: '15px',
		fontSize: '.6rem',
		color: scssVars.colors.pink,
		lineHeight: '1rem',
		width: '350px'
	},
	zipcode: {
		textfield: {
			backgroundColor: scssVars.colors.whiteGray,
			width: '130px',
			borderRadius: '10px',
			paddingLeft: '15px',
			fontSize: '1.6em',
		},
		input: {
			color: 'black'
		}
	}
};

export default styles;
