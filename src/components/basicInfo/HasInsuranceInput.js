import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import radioStyles from '../common/RadioButton.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import RadioButtonIcons from '../common/RadioButtonIcons';

/**
 * @function HasInsuranceInput
 * @param {object} props
 * @description
 *   This component displays the HasInsurance Input on the form.
 * */
const HasInsuranceInput = (props) => {
	const {
		titleText, hasCurrentPolicy, updateState, disabled, errorText, propIsInvalid
	} = props;
	const {checkedIcon, uncheckedIcon} = RadioButtonIcons;

	return (
		<Row className="hasInsuranceInputs">
			<Col xs="12">
				<Title titleText={titleText}/>
			</Col>
			<Col xs="12">
				<RadioButtonGroup
					name="hasCurrentPolicy"
					className="flex"
					onChange={(event, value) => {
						updateState({target: {name: 'hasCurrentPolicy', value}});
					}}
				>
					<RadioButton
						checkedIcon={checkedIcon}
						uncheckedIcon={uncheckedIcon}
						iconStyle={radioStyles.radioButtonIcon}
						labelStyle={radioStyles.radioButtonLabel}
						style={radioStyles.radioButton}
						value="noPolicy"
						label="No"
						disabled={disabled}
					/>
					<RadioButton
						checkedIcon={checkedIcon}
						uncheckedIcon={uncheckedIcon}
						iconStyle={radioStyles.radioButtonIcon}
						labelStyle={radioStyles.radioButtonLabel}
						style={radioStyles.radioButton}
						value="hasPolicy"
						label="Yes"
						disabled={disabled}
					/>
				</RadioButtonGroup>
			</Col>
			<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
		</Row>
	);
};

HasInsuranceInput.propTypes = {
	hasCurrentPolicy: React.PropTypes.string,
	updateState: React.PropTypes.func.isRequired,
	titleText: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	errorText: React.PropTypes.string,
	propIsInvalid: React.PropTypes.bool
};

export default HasInsuranceInput;
