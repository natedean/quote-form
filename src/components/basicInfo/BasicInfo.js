import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import ZipcodeInput from './ZipcodeInput';
import HasInsuranceInput from './HasInsuranceInput';
import BirthdateInput from './BirthdateInput';
import GenderInput from './GenderInput';
import StepButton from '../common/StepButton';

/**
 * @function - BasicInfo
 * @param {object} props
 * @description
 *   This component is responsible for the Basic Info Inputs
 * */
const BasicInfo = (props) => {
	const {
		currentFormEntry, actions, headerText, subHeaderText, showCurrentSectionErrors,
		zipcodeErrorText, quoteIsBeingRequested, drawerIsOpen,
	} = props;
	const nextSectionIndex = 1;

	return (
		<div className={`basicInfo contentContainer${drawerIsOpen ? ' contentContainer--mobileDrawerOpen' : ''}`}>
			<Row className="basicInfo__header">
				<Col xs="12">
					<h2>{headerText}</h2>
					<div className="h5">{subHeaderText}</div>
				</Col>
			</Row>
			<Row>
				<Col xs="12" md="6">
					<ZipcodeInput
						titleText="Zipcode"
						propIsInvalid={(showCurrentSectionErrors && zipcodeErrorText)
						|| (showCurrentSectionErrors && !currentFormEntry['zipcode']) ||
						(showCurrentSectionErrors && currentFormEntry['zipcode'] && currentFormEntry['zipcode'].length < 5)}
						errorText={zipcodeErrorText || 'Please enter a valid Zipcode'}
						zipcode={currentFormEntry['zipcode']}
						updateState={(event) => actions.zipcodeChanged(event)}
						disabled={quoteIsBeingRequested}
					/>
					<HasInsuranceInput
						titleText="Do you currently have a life insurance policy?"
						propIsInvalid={showCurrentSectionErrors && !currentFormEntry['hasCurrentPolicy']}
						errorText="Please make a selection"
						hasCurrentPolicy={currentFormEntry['hasCurrentPolicy']}
						updateState={(event) => actions.hasCurrentPolicyChanged(event)}
						disabled={quoteIsBeingRequested}
					/>
				</Col>
				<Col xs="12" md="6" className="basicInfo__rightColumn">
					<BirthdateInput
						titleText="Birthdate"
						errorText="Please enter a valid Birthdate"
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['birthdate'])
						|| (showCurrentSectionErrors && currentFormEntry['birthdate']
						&& Object.keys(currentFormEntry['birthdate']).length < 3)}
						birthdate={currentFormEntry['birthdate']}
						updateState={(event) => actions.birthdateChanged(event)}
						disabled={quoteIsBeingRequested}
					/>
					<GenderInput
						titleText="Gender"
						errorText="Please make a selection"
						propIsInvalid={(showCurrentSectionErrors && !currentFormEntry['gender'])}
						gender={currentFormEntry['gender']}
						updateState={(event) => actions.genderChanged(event)}
						disabled={quoteIsBeingRequested}
					/>
				</Col>
			</Row>
			<Row>
				<Col xs="12" className="text--center mt-2">
					<StepButton
						disabled={showCurrentSectionErrors}
						updateSection={(event) => actions.updateSection(nextSectionIndex)}
						label="Next"
					/>
				</Col>
			</Row>
		</div>
	);
};

BasicInfo.propTypes = {
	headerText: React.PropTypes.string,
	subHeaderText: React.PropTypes.string,
	actions: React.PropTypes.object,
	currentFormEntry: React.PropTypes.object,
	zipcodeErrorText: React.PropTypes.string,
	drawerIsOpen: React.PropTypes.bool,
	quoteIsBeingRequested: React.PropTypes.bool,
	showCurrentSectionErrors: React.PropTypes.bool,
};

export default BasicInfo;
