import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import HealthRatingInput from './HealthRatingInput';
import HasUsedNicotineInput from './HasUsedNicotineInput';
import BodyInput from './BodyInput';
import StepButton from '../common/StepButton';

/**
 * @function HealthInfo
 * @param {object} props
 * @description
 *   This is responsible for the Health related Inputs
 * */
const HealthInfo = (props) => {
	const {
		currentFormEntry, headerText, subHeaderText, actions,
		quoteIsBeingRequested, drawerIsOpen, showCurrentSectionErrors
	} = props;

	const previousSectionIndex = 0;
	const nextSectionIndex = 2;

	return (
		<div className={`healthInfo contentContainer${drawerIsOpen ? ' contentContainer--mobileDrawerOpen' : ''}`}>
			<Row className="healthInfo__header">
				<Col xs="12">
					<h2>{headerText}</h2>
					<div className="h5">{subHeaderText}</div>
				</Col>
			</Row>
			<Row>
				<Col xs="12" md="6">
					<HealthRatingInput
						disabled={quoteIsBeingRequested}
						healthRating={currentFormEntry['healthRating']}
						titleText="How would you rate your health?"
						updateState={(event) => actions.healthRatingChanged(event)}
						onDragStop={actions.sliderDragStopped}
					/>
				</Col>
				<Col xs="12" md="6" className="healthInfo__rightColumn">
					<HasUsedNicotineInput
						propIsInvalid={showCurrentSectionErrors && !currentFormEntry['hasUsedNicotine']}
						errorText="Please make a selection"
						hasUsedNicotine={currentFormEntry['hasUsedNicotine']}
						titleText="Do you currently use any tobacco or nicotine products?"
						updateState={(event) => actions.hasUsedNicotineChanged(event)}
						disabled={quoteIsBeingRequested}
					/>
					<BodyInput
						isSectionInProgress={showCurrentSectionErrors}
						userHeight={currentFormEntry['height']}
						userWeight={currentFormEntry['weight']}
						actions={actions}
						titleText="Body"
						disabled={quoteIsBeingRequested}
					/>
				</Col>
			</Row>
			<Row>
				<Col xs="12" sm="6" className="text--center mt-2">
					<StepButton
						updateSection={(event) => actions.updateSection(previousSectionIndex)}
						label="Previous"
					/>
				</Col>
				<Col xs="12" sm="6" className="text--center mt-2">
					<StepButton
						disabled={showCurrentSectionErrors}
						updateSection={(event) => actions.updateSection(nextSectionIndex)}
						label="Next"
					/>
				</Col>
			</Row>
		</div>
	);
};

HealthInfo.propTypes = {
	headerText: React.PropTypes.string,
	subHeaderText: React.PropTypes.string,
	currentFormEntry: React.PropTypes.object,
	actions: React.PropTypes.object,
	quoteIsBeingRequested: React.PropTypes.bool,
	drawerIsOpen: React.PropTypes.bool,
	showCurrentSectionErrors: React.PropTypes.bool
};

export default HealthInfo;
