import getMuiTheme from 'material-ui/styles/getMuiTheme';
const muiTheme = getMuiTheme({
    slider: {
        trackSize: 3,
        trackColor: '#b4b4b4',
        trackColorSelected: '#a4b4c5',
        handleSize: 29,
        handleSizeDisabled: 20,
        handleSizeActive: 20,
        handleColorZero: '#e29113',
        handleFillColor: '#e29113',
        selectionColor: '#e29113',
        rippleColor: 'transparent',
    }
});

const styles = {
    muiTheme,
    selectedLabel: (healthRating, comparisonValue) => {
        return Object.assign({},(healthRating === comparisonValue) ? {color: '#e29113'} : {});
    },
    zipcode: {
        textfield: {
            backgroundColor: '#eae8e8',
            width: '130px',
            borderRadius: '10px',
            paddingLeft: '15px',
            fontSize: '1.6em',
        }
    }
};

export default styles;
