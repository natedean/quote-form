import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import Slider from 'material-ui/Slider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styles from './HealthInfo.styles';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function HealthRatingInput
 * @param {object} props
 * @description
 *   This is the Custom Slider input for Health Rating
 * */
const HealthRatingInput = (props) => {
	const {titleText, healthRating, updateState, disabled, onDragStop} = props;

	return (
		<div className="healthRatingInput">
			<Row className="healthRatingInput__header">
				<Col xs="12">
					<h4>{titleText}</h4>
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<MuiThemeProvider muiTheme={styles.muiTheme}>
						<Slider
							className="healthRatingInput__slider"
							disabled={disabled}
							min={.1}
							max={.4}
							step={.1}
							defaultValue={healthRating}
							onDragStop={() => onDragStop()}
							onChange={(event, value) => {
								updateState({target: {name: 'healthRating', value}});
							}}
							onFocus={(event) => scrollToInput({target: {name: 'healthRating', offsetParent: event.target.offsetParent}})}
						/>
					</MuiThemeProvider>
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<div className="healthRatingInput__ratingLabelsContainer">
						<div className="healthRatingInput__ratingLabel" style={styles.selectedLabel(healthRating, .1)}>
							Fair
						</div>
						<div className="healthRatingInput__ratingLabel" style={styles.selectedLabel(healthRating, .2)}>
							Good
						</div>
						<div className="healthRatingInput__ratingLabel" style={styles.selectedLabel(healthRating, .3)}>
							Very Good
						</div>
						<div className="healthRatingInput__ratingLabel" style={styles.selectedLabel(healthRating, .4)}>
							Excellent
						</div>
					</div>
				</Col>
			</Row>
		</div>
	);
};

HealthRatingInput.propTypes = {
	healthRating: React.PropTypes.number,
	updateState: React.PropTypes.func,
	titleText: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	onDragStop: React.PropTypes.func
};

export default HealthRatingInput;
