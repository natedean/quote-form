import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import radioStyles from '../common/RadioButton.styles';
import Title from '../common/Title';
import ErrorText from '../common/ErrorText';
import RadioButtonIcons from '../common/RadioButtonIcons';

/**
 * @function HasUsedNicotineInput
 * @param {object} props
 * @description
 *   This Input is used for toggling the HasUsedNicotine field
 * */
const HasUsedNicotineInput = (props) => {
	const {
		titleText, hasUsedNicotine, updateState,
		disabled, propIsInvalid, errorText
	} = props;
	const {checkedIcon, uncheckedIcon} = RadioButtonIcons;

	return (
		<div className="hasUsedNicotineInput">
			<Row>
				<Col xs="12">
					<Title titleText={titleText} />
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<RadioButtonGroup
						name="hasUsedNicotine"
						className="flex"
						defaultSelected={hasUsedNicotine}
						onChange={(event, value) => {
							updateState({target: {name: 'hasUsedNicotine', value}});
						}}
					>
						<RadioButton
							checkedIcon={checkedIcon}
							uncheckedIcon={uncheckedIcon}
							iconStyle={radioStyles.radioButtonIcon}
							labelStyle={radioStyles.radioButtonLabel}
							style={radioStyles.radioButton}
							value="hasNotUsed"
							label="No"
							disabled={disabled}
						/>
						<RadioButton
							checkedIcon={checkedIcon}
							uncheckedIcon={uncheckedIcon}
							iconStyle={radioStyles.radioButtonIcon}
							labelStyle={radioStyles.radioButtonLabel}
							style={radioStyles.radioButton}
							value="hasUsed"
							label="Yes"
							disabled={disabled}
						/>
					</RadioButtonGroup>
				</Col>
				<ErrorText showErrorText={propIsInvalid} errorText={errorText}/>
			</Row>
		</div>
	);
};

HasUsedNicotineInput.propTypes = {
	hasUsedNicotine: React.PropTypes.string,
	updateState: React.PropTypes.func,
	titleText: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	propIsInvalid: React.PropTypes.bool,
	errorText: React.PropTypes.string
};

export default HasUsedNicotineInput;
