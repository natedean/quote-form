import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import CustomSelectField from '../common/CustomSelectField';
import weightFormatter from '../../services/weightFormatters';
import heightFormatter from '../../services/heightFormatter';
import ErrorText from '../common/ErrorText';
import Title from '../common/Title';
import scrollToInput from '../../services/scrollToInput';

/**
 * @function BodyInput
 * @param {object} props
 * @description
 *   This is responsible for the Height and Weight Inputs
 * */
const BodyInput = (props) => {
	const {
		titleText, userHeight, userWeight, actions, disabled, isSectionInProgress
	} = props;

	const weightRange = weightFormatter(50, 800);
	const heightRange = heightFormatter(4, 8);
	const weightErrorText = 'Please enter your Weight';
	const heightErrorText = 'Please enter your Height';

	return (
		<div className="bodyInput">
			<Row>
				<Col xs="12">
					<Title titleText={titleText}/>
				</Col>
			</Row>
			<Row>
				<Col xs="6" className="Select-wrapper">
					<CustomSelectField
						placeholder="Height"
						name="height"
						value={userHeight}
						disabled={disabled}
						collection={heightRange}
						updateState={(event) => {
							actions.heightChanged({
								target: {
									name: 'height',
									value: event.value
								}
							});
						}}
						onOpen={() => scrollToInput({default: {name: 'bodyInput'}})}
					/>
					<ErrorText errorText={heightErrorText} showErrorText={isSectionInProgress && !userHeight}/>
				</Col>
				<Col xs="6" className="Select-wrapper">
					<CustomSelectField
						placeholder="Weight"
						name="weight"
						value={userWeight}
						collection={weightRange}
						disabled={disabled}
						updateState={(event) => {
							actions.weightChanged({
								target: {
									name: 'weight',
									value: event.value
								}
							});
						}}
						onOpen={() => scrollToInput({default: {name: 'bodyInput'}})}
					/>
					<ErrorText errorText={weightErrorText} showErrorText={isSectionInProgress && !userWeight}/>
				</Col>
			</Row>
		</div>
	);
};

BodyInput.propTypes = {
	titleStyle: React.PropTypes.object,
	titleText: React.PropTypes.string,
	userWeight: React.PropTypes.oneOfType([
		React.PropTypes.object,
		React.PropTypes.number
	]),
	userHeight: React.PropTypes.oneOfType([
		React.PropTypes.object,
		React.PropTypes.string
	]),
	updateState: React.PropTypes.func,
	actions: React.PropTypes.object,
	disabled: React.PropTypes.bool,
	isSectionInProgress: React.PropTypes.bool,
};

export default BodyInput;
