import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {grey100} from 'material-ui/styles/colors';
import BigQuoteBarContainer from '../containers/BigQuoteBarContainer';
import MobileQuoteBarContainer from '../containers/MobileQuoteBarContainer';
import FooterContainer from '../containers/FooterContainer';
import QuoteFormContainer from '../containers/QuoteFormContainer';
import parseQueryString from '../services/parseQueryString';
import pushQueryPropsToState from '../services/pushQueryPropsToState';
import addScrollToInputEvents from '../services/addScrollToInputEvents';

const muiTheme = getMuiTheme({
	palette: {
		primary1Color: '#eeeeee',
		textColor: grey100
	},
	fontFamily: 'Open Sans, sans-serif'
});

/**
 * @class App
 * @param {object} actions - Actions tied to this component.
 * @param {object} props - see propTypes below
 * @description
 * The component that contains the entire App.
 */
class App extends React.Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		const {location, actions} = this.props;
		pushQueryPropsToState(parseQueryString(location), actions);
	}

	componentDidMount () {
		addScrollToInputEvents();
	}

	componentWillReceiveProps(props) {
		if (props.redirectLink && props.redirectLink.length > 10) {
			window.location.replace(props.redirectLink);
		}
	}

	render() {
		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				<div>
					<BigQuoteBarContainer/>
					<MobileQuoteBarContainer/>
					<QuoteFormContainer/>
					<FooterContainer/>
				</div>
			</MuiThemeProvider>
		);
	}
}

App.propTypes = {
	actions: React.PropTypes.object,
	redirectLink: React.PropTypes.string,
	location: React.PropTypes.object
};

export default App;
