import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';

/**
 * @const QuoteDisplay
 * @param {object} props - see PropTypes below
 * @description
 * Displaying the progress step the user is currently on
 */
const QuoteDisplay = (props) => {
	const {currentSection} = props;
	const stepNumberToShow = currentSection + 1;

	return (
		<div className="quoteDisplay">
			<Row className="text--center">
				<Col xs="12">
					<div className="quoteDisplay__stepProgress">
						Step {stepNumberToShow} of 3
					</div>
				</Col>
				<Col xs="12">
					<div className="quoteDisplay__encouragingTextContainer">
						Keep going to see how much lower your rates can be.
					</div>
				</Col>
			</Row>
		</div>
	);
};

QuoteDisplay.propTypes = {
	currentSection: React.PropTypes.number
};

export default QuoteDisplay;
