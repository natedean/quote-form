import React from 'react';
import Col from '../common/Col';
import Row from '../common/Row';
import QuoteBarHeader from './QuoteBarHeader';
import PolicyValueInput from './PolicyValueInput';
import TermLengthInput from './TermLengthInput';
import QuoteDisplay from './QuoteDisplay';
import StepDisplay from './StepDisplay';
import styles from './BigQuoteBar.styles';
import {helpers} from './BigQuoteBar.lifecyle';
import maskPhoneNumber from '../../services/maskPhoneNumber';

/**
 * @function BackgroundBanner
 * @param {object} props
 * @description
 * This component contains all of the logic and content of the bigQuoteBar
 */

class BigQuoteBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = helpers.getInitialComponentState(props);
		this.onScrollEvent = this.onScrollEvent.bind(this);
	}

	componentDidMount() {
		window.addEventListener('scroll', this.onScrollEvent);
		this.onScrollEvent();
	}

	componentWillReceiveProps(props) {
		helpers.componentWillReceiveProps(props, this);
	}

	componentWillUnmount(props) {
		window.removeEventListener('scroll', this.onScrollEvent);
	}

	onScrollEvent() {
		const scrollMax = 102;
		const scrollTop = pageYOffset;
		const shouldBeSticky = (scrollTop > scrollMax);
		this.setState({shouldBeSticky});
	}

	render() {
		const {
			policyValue, termLength, quoteIsBeingRequested, actions,
			tollFreeNumber, currentSection
		} = this.props;
		const {shouldBeSticky} = this.state;

		return (
			<Row>
				<Col xs="12" className="noPadding">
					<div className="quoteBar">
						<QuoteBarHeader
							message="Speak to a Life Insurance Agent"
							contactNumber={maskPhoneNumber(tollFreeNumber, false)}
						/>
						<div className="quoteBar__orangeWrapper">
							<div className="quoteBar__orangeWrapperBackground quoteBar__orangeWrapperBackground--leftSide"/>
							<div className="quoteBar__orangeWrapperBackground quoteBar__orangeWrapperBackground--rightSide"/>
							<Row
								style={styles.inputsContainer}
								className={`orangeInputsContainer ${shouldBeSticky ? 'orangeInputsContainer--fixed' : ''}`}
							>
								<Col xs="4" className="noPadding">
									<PolicyValueInput
										policyValue={policyValue}
										handleHelperPopover={(event, popoverToShow) => helpers.handleHelperPopover(event, popoverToShow, this)}
										handleRequestClose={(popoverToClose) => helpers.handleRequestClose(popoverToClose, this)}
										updateState={(event) => helpers.updateState(event, this)}
										anchorEl={this.state.anchorEl}
										showPopover={this.state.showPopover.policyValue}
										quoteIsBeingRequested={quoteIsBeingRequested}
										onDragStop={actions.sliderDragStopped}
									/>
								</Col>
								<Col xs="4" className="noPadding">
									<TermLengthInput
										termLength={termLength}
										handleHelperPopover={(event, popoverToShow) => helpers.handleHelperPopover(event, popoverToShow, this)}
										handleRequestClose={(popoverToClose) => helpers.handleRequestClose(popoverToClose, this)}
										updateState={(event) => helpers.updateState(event, this)}
										anchorEl={this.state.anchorEl}
										showPopover={this.state.showPopover.termLength}
										quoteIsBeingRequested={quoteIsBeingRequested}
										onDragStop={actions.sliderDragStopped}
									/>
								</Col>
								<Col xs="4" className="noPadding justifyContent--center">
									<QuoteDisplay
										currentSection={currentSection}
									/>
								</Col>
							</Row>
						</div>
						<StepDisplay/>
					</div>
				</Col>
			</Row>
		);
	}
}

BigQuoteBar.propTypes = {
	policyValue: React.PropTypes.number,
	actions: React.PropTypes.object,
	quoteAmountMonth: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]).isRequired,
	quoteAmountYear: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]).isRequired,
	termLength: React.PropTypes.number,
	quoteIsBeingRequested: React.PropTypes.bool,
	tollFreeNumber: React.PropTypes.string,
	currentSection: React.PropTypes.number
};

export default BigQuoteBar;
