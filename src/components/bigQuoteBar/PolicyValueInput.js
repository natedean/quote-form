import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Slider from 'material-ui/Slider';
import policyValueFromInternal from '../../services/policyValueFromInternal';
import stylePolicyValue from '../../services/stylePolicyValue';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styles from './BigQuoteBar.styles';
import { getSliderValue, getPolicyValue, sliderMin, sliderMax, sliderStep  } from '../../services/policySlider';

/**

 * @const PolicyValueInput
 * @param {object} props - see PropTypes below
 * @description
 * Creates the PolicyValueInput slider along with tooltips and popovers
 */
const PolicyValueInput = (props) => {
	const {
		policyValue, handleHelperPopover, handleRequestClose, onDragStop,
		showPopover, anchorEl, updateState, quoteIsBeingRequested
	} = props;

	return (
		<div className="policyValueInput">
			<Row>
				<Col xs="12">
					<Row className="alignItems--center noMargin">
						<Col xs="6" className="noPadding">
							<div className="policyValueInput__inputValue">
								<sup className="policyValueInput__dollarSign">$</sup>
								{`${stylePolicyValue(policyValueFromInternal(policyValue))}`}
							</div>
						</Col>
						<Col xs="6" className="alignItems--flex-end noPadding">
							<Row className="alignItems--center">
								<div className="policyValueInput__inputLabel">
									<div>Choose</div>
									<div>policy</div>
									<div>value</div>
								</div>

								<IconButton
									onMouseOver={(event) => handleHelperPopover(event, 'policyValue')}
									style={styles.helpButton}
									iconStyle={styles.helpIcon}>
									<HelpIcon />
								</IconButton>
								<Popover
									style={styles.helpPopover}
									open={showPopover}
									anchorEl={anchorEl}
									anchorOrigin={styles.popoverAnchor}
									targetOrigin={styles.popoverTarget}
									onRequestClose={() => handleRequestClose('policyValue')}
									className="popOver"
								>
									<div className="popOver__container">
										<p>
											A good rule of thumb is to multiply your
											salary x10.
										</p>
									</div>
								</Popover>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<MuiThemeProvider muiTheme={styles.muiTheme}>
						<Slider
							name="insuranceAmount"
							disabled={quoteIsBeingRequested}
							className="policyValueInput__slider slider"
							min={sliderMin}
							max={sliderMax}
							step={sliderStep}
							value={getSliderValue(policyValue)}
							onDragStop={() => onDragStop()}
							onChange={(event, sliderValue) => {
								updateState({
									target: {
										name: 'policyValue',
										value: getPolicyValue(sliderValue)
									}
								});
							}}
						/>
					</MuiThemeProvider>
				</Col>
			</Row>
		</div>
	);
};

PolicyValueInput.propTypes = {
	policyValue: React.PropTypes.number,
	handleHelperPopover: React.PropTypes.func,
	handleRequestClose: React.PropTypes.func,
	updateState: React.PropTypes.func,
	anchorEl: React.PropTypes.object,
	showPopover: React.PropTypes.bool,
	quoteIsBeingRequested: React.PropTypes.bool,
	onDragStop: React.PropTypes.func
};

export default PolicyValueInput;
