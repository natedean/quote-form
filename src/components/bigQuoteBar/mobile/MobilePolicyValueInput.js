import React from 'react';
import Row from '../../common/Row';
import Col from '../../common/Col';
import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Slider from 'material-ui/Slider';
import policyValueFromInternal from '../../../services/policyValueFromInternal';
import stylePolicyValue from '../../../services/stylePolicyValue';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { getSliderValue, getPolicyValue, sliderMin, sliderMax, sliderStep  } from '../../../services/policySlider';
import styles from './MobileQuoteBar.styles';

/**
 * @constant MobilePolicyValueInput
 * @param {object} props - see propTypes below
 * @description
 * This is the Mobile version of the Policy Value input on the Quote Bar
 * */
const MobilePolicyValueInput = (props) => {
	const {
		policyValue, handleHelperPopover, handleRequestClose,
		showPopover, anchorEl, updateState, onDragStop
	} = props;

	return (
		<Row className="mobilePolicyValueInput">
			<div className="flex flex--spaceAround">
				<div className="marginAuto">
					<span style={
						styles.dollarWrapper(stylePolicyValue(policyValueFromInternal(policyValue)).length)
					}>
						$
					</span>
					<span className="mobilePolicyValueInput__policyLabel">
						{`${stylePolicyValue(policyValueFromInternal(policyValue))}`}
					</span>
				</div>
				<div className="mobilePolicyValueInput__policyValue">
					<div>Choose</div>
					<div>policy</div>
					<div>value</div>
				</div>
				<div className="mobilePolicyValueInput__helpIcon">
					<IconButton
						onTouchTap={(event) => handleHelperPopover(event, 'policyValue')}
						style={styles.helpButton}
						iconStyle={styles.helpIcon}>
						<HelpIcon />
					</IconButton>
					<Popover
						style={styles.helpPopover}
						open={showPopover}
						anchorEl={anchorEl}
						anchorOrigin={styles.popoverAnchor}
						targetOrigin={styles.popoverTarget}
						onRequestClose={() => handleRequestClose('policyValue')}
					>
						<div className={styles.classNames.popoverContainer}>
							<p>
								A good rule of thumb is to multiply your salary x10.
							</p>
						</div>
					</Popover>
				</div>
			</div>
			<Col xs="12">
				<div className="mobilePolicyValueInput__sliderLabels">
					<span className="pull--left">$100K</span>
					<span className="pull--right">$10M</span>
				</div>
				<MuiThemeProvider muiTheme={styles.muiTheme}>
					<Slider
						name="insuranceAmount"
						className="mobilePolicyValueInput__slider slider"
						min={sliderMin}
						max={sliderMax}
						step={sliderStep}
						value={getSliderValue(policyValue)}
						onDragStop={() => onDragStop()}
						onChange={(event, sliderValue) => {
							updateState({
								target: {
									name: 'policyValue',
									value: getPolicyValue(sliderValue)
								}
							});
						}}
					/>
				</MuiThemeProvider>
			</Col>
		</Row>
	);
};

MobilePolicyValueInput.propTypes = {
	policyValue: React.PropTypes.number,
	handleHelperPopover: React.PropTypes.func,
	handleRequestClose: React.PropTypes.func,
	updateState: React.PropTypes.func,
	anchorEl: React.PropTypes.object,
	showPopover: React.PropTypes.bool,
	onDragStop: React.PropTypes.func
};

export default MobilePolicyValueInput;
