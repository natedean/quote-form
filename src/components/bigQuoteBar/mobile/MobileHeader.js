import React from 'react';
import Row from '../../common/Row';
import Col from '../../common/Col';
import PhoneIcon from 'material-ui/svg-icons/hardware/phone-iphone';
import MobileNavigation from './MobileNavigation';
import IconButton from 'material-ui/IconButton';
import styles from './MobileQuoteBar.styles';
import constants from '../../../constants/generalConstants';
let scssVars = require('../../../styles/scss/base/variables/variables.json');
import links from '../../../constants/textLinks';

/* eslint-disable */
import logo from '../../../images/SQ_logo-white.png';
/* eslint-enable */

/**
 * @constant MobileHeader
 * @param {object} props - see propTypes below
 * @description
 * This is the Header for the Mobile Quote Bar
 * */
const MobileHeader = (props) => {
	const {searchTooltip, tollFreeNumber} = props;

	return (
		<Row className="mobileHeader">
			<Col xs="2">
				<MobileNavigation/>
			</Col>
			<Col xs="8">
				<a href={links.home}>
					<img className="mobileHeader__logo" src={logo}/>
				</a>
			</Col>
			<Col xs="2" className="mobileHeader__icon">
				<IconButton
					href={`tel:${tollFreeNumber}`}
					tooltip={searchTooltip}
					style={styles.quoteBarHeader.phoneButton}
					iconStyle={styles.quoteBarHeader.phoneButtonIcon}
				>
					<PhoneIcon color={scssVars.colors.aqua}/>
				</IconButton>
			</Col>
		</Row>
	);
};

MobileHeader.propTypes = {
	searchTooltip: React.PropTypes.string,
	tollFreeNumber: React.PropTypes.string
};

export default MobileHeader;
