import getMuiTheme from 'material-ui/styles/getMuiTheme';
let scssVars = require('../../../styles/scss/base/variables/variables.json');

const defaults = {
	spacing: '15px',
	textAlign: 'middle',
	quoteBarHeight: '420px',
	noSpace: 0
};

const muiTheme = getMuiTheme({
	slider: {
		trackSize: 2,
		trackColor: 'white',
		trackColorSelected: 'white',
		handleSize: 20,
		handleSizeDisabled: 8,
		handleSizeActive: 20,
		handleColorZero: 'white',
		handleFillColor: 'white',
		selectionColor: 'white',
		rippleColor: 'transparent',
	}
});

const styles = {
	muiTheme,
	classNames: {
		quoteBarLabels: 'quoteBarLabels',
		quoteBarInputs: 'quoteBarInputs',
		labelColumns: 'labelColumns',
		policyInput: 'policyInput',
		termInput: 'termInput',
		estimateLabel: 'estimateLabel',
		contactNumber: 'contactNumber',
		quoteBarHeaderMessage: 'quoteBarHeaderMessage',
		popoverContainer: 'popOver__container'
	},
	flexContainer : {
		display:'flex',
		flexDirection:'row',
		alignItems:'baseline',
		marginLeft:'15px',
		marginRight:'15px'
	},
	flexItem: {
		margin:'auto'
	},
	overlay: {
		backgroundColor: 'transparent'
	},
	container: {
		marginLeft: defaults.noSpace,
		marginRight: defaults.noSpace,
	},
	column: {
		paddingLeft: defaults.noSpace,
		paddingRight: defaults.noSpace
	},
	inputsContainer: {
		height: '190px',
		marginTop: '10px'
	},
	mediaLayer: {
		height: defaults.quoteBarHeight
	},
	image: {
		height: defaults.quoteBarHeight
	},
	divider: {
		position: 'absolute',
		top: '50px',
		left: '20px',
		height: '33px',
		borderLeft: '1px solid white'
	},
	labelColumns: {
		marginTop: defaults.spacing
	},
	helpIcon: {
		width: 24,
		height: 24
	},
	helpButton: {
		marginTop: 0
	},
	helpPopover: {
		fontSize: '.8em',
		borderRadius: defaults.spacing,
		border: '2px solid orange'
	},
	popoverAnchor: {
		horizontal: 'left',
		vertical: 'bottom'
	},
	popoverTarget: {
		horizontal: 'left',
		vertical: 'top'
	},
	popoverContainer: {
		borderRadius: defaults.spacing,
		color: 'black',
		backgroundColor: 'white',
		padding: '10px 40px 10px 40px'
	},
	labelContainer: {
		textAlign: 'center'
	},
	dollarWrapper: () => {
		return {
			fontSize:'1.2em',
			color:'white',
			paddingLeft:'15px'
		};
	},
	componentDivider: {
		position: 'absolute',
		top: '45px',
		left: '87.5%',
		height: '125px',
		borderLeft: '1px solid white'
	},
	quoteDisplay: {
		container: {
			letterSpacing: 0,
			marginTop: 0
		},
		monthLabel: {
			fontSize: '1.6em',
		},
		encouragingTextContainer: {
			textAlign: 'center',
			fontSize: '1.1em',
			lineHeight: '1.15em',
			fontWeight: 'bold',
			marginTop: '60px',
			marginLeft: '25%'
		},
		encouragingText: {
			maxWidth:'50px'
		}
	},
	quoteBarHeader: {
		logo: {
			width:'200px'
		},
		phoneButton: {
			backgroundColor: 'white',
			borderRadius: '30px',
			height:'35px',
			width: '35px'
		},
		phoneButtonIcon: {
			width:'24px',
			height:'24px',
			marginLeft:'-6px',
			marginTop:'-6px'
		},
		navButton: {
			height:'35px',
			width: '35px'
		},
		navButtonIcon: {
			width:'30px',
			height:'30px',
			margin: '-12px',
		},
		divider: {
			paddingTop: '15px',
			fontSize: '1.7em'
		},
		navMenu: {
			background: scssVars.colors.black
		},
		navMenuLink: {
			textDecoration: 'none'
		},
		navMenuItem: {
			color: 'white'
		}
	},
	arrow: {
		container: {
			width: 45,
			height: 45,
			padding: 0,
			margin: '0 auto'
		},
		icon: {
			width: 48,
			height: 48,
			color: scssVars.colors.sienna,
			transition: 'all .5s linear'
		}
	},
};

export default styles;
