import React from 'react';
import NavIcon from 'material-ui/svg-icons/navigation/menu';
import IconButton from 'material-ui/IconButton';
import styles from './MobileQuoteBar.styles';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import links from '../../../constants/textLinks';
import {bindAll} from 'lodash';

/**
 * @class MobileNavigation
 * @param {object} props - see propTypes below
 * @description
 * This is the navigation section for the mobile hamburger button and the mobile menu items it has available
 * */

export default class MobileNavigation extends React.Component {

	constructor(props) {
		super(props);
		this.state = {open: false};
		bindAll(
			this,
			'toggleMenu'
		);
	}

	/**
	 * @function toggleMenu
	 * @description
	 * Function for changing state of navigation drawer from open to closed
	 */
	toggleMenu() {
		this.setState({open: !this.state.open});
	}

	render() {
		return (
			<div>
				<IconButton
					style={styles.quoteBarHeader.navButton}
					iconStyle={styles.quoteBarHeader.navButtonIcon}
					label="Open Drawer"
					onClick={this.toggleMenu}
				>
					<NavIcon color=""/>
				</IconButton>
				<Drawer
					docked={false}
					width={250}
					open={this.state.open}
					disableSwipeToOpen={true}
					containerStyle={styles.quoteBarHeader.navMenu}
					onRequestChange={(open) => this.setState({open})}
					className="mobileHeader__navigationDrawer"
				>
					<a href={links.footer.lifeBasics.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.lifeBasics.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.howMuch.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.howMuch.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.howDoesItWork.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.howDoesItWork.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.sqDifference.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.sqDifference.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.blog.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.blog.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.otherInsurance.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.otherInsurance.mobileText}
						</MenuItem>
					</a>
					<a href={links.footer.contactUs.textLink} style={styles.quoteBarHeader.navMenuLink}>
						<MenuItem style={styles.quoteBarHeader.navMenuItem} onTouchTap={this.toggleMenu}>
							{links.footer.contactUs.mobileText}
						</MenuItem>
					</a>
				</Drawer>
			</div>
		);
	}
}
