import React from 'react';
import Row from '../../common/Row';
import Col from '../../common/Col';
import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Slider from 'material-ui/Slider';
import termLengthFromInternal from '../../../services/termLengthFromInternal';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styles from './MobileQuoteBar.styles';
import links from '../../../constants/textLinks';

/**
 * @constant MobileTermLengthInput
 * @param {object} props - see propTypes below
 * @description
 * This is the mobile version of the Term Length Input
 * */
const MobileTermLengthInput = (props) => {
	const {
		termLength, handleHelperPopover, handleRequestClose,
		showPopover, anchorEl, updateState, onDragStop
	} = props;

	return (
		<Row className="mobileTermLengthInput">
			<div className="flex flex--spaceAround">
				<div className="mobileTermLengthInput__termValue">
					{`${termLengthFromInternal(termLength).toFixed(0)} Years`}
				</div>
				<div className="mobileTermLengthInput__termLabel">
					<div>Choose</div>
					<div>term</div>
					<div>length</div>
				</div>
				<div className="marginAuto">
					<IconButton
						onTouchTap={(event) => handleHelperPopover(event, 'termLength')}
						style={Object.assign({}, styles.helpButton, {paddingLeft: '18px'})}
						iconStyle={styles.helpIcon}>
						<HelpIcon />
					</IconButton>
					<Popover
						style={styles.helpPopover}
						open={showPopover}
						anchorEl={anchorEl}
						anchorOrigin={styles.popoverAnchor}
						targetOrigin={styles.popoverTarget}
						onRequestClose={() => handleRequestClose('termLength')}
					>
						<div className={styles.classNames.popoverContainer}>
							<p>
								More affordable than other types of insurance, a term life policy covers you for a specified period of time-usually 10, 20, or 30 years.
							</p>
						</div>
					</Popover>
				</div>
			</div>
			<Col xs="12">
				<div className="mobileTermLengthInput__sliderLabels">
					<span className="pull--left">10 years</span>
					<span className="pull--right">30 years</span>
				</div>
				<MuiThemeProvider muiTheme={styles.muiTheme}>
					<Slider
						name="termLength"
						className="mobileTermLengthInput__slider slider"
						value={termLength}
						min={.1}
						max={.3}
						step={.05}
						onDragStop={() => onDragStop()}
						onChange={(event, value) => {
							updateState({
								target: {
									name: 'termLength',
									value
								}
							});
						}}
					/>
				</MuiThemeProvider>
			</Col>
		</Row>
	);
};

MobileTermLengthInput.propTypes = {
	termLength: React.PropTypes.number,
	handleHelperPopover: React.PropTypes.func,
	handleRequestClose: React.PropTypes.func,
	updateState: React.PropTypes.func,
	anchorEl: React.PropTypes.object,
	showPopover: React.PropTypes.bool,
	onDragStop: React.PropTypes.func
};

export default MobileTermLengthInput;
