import React from 'react';
import Row from '../../common/Row';
import Col from '../../common/Col';
import MobileHeader from './MobileHeader';
import MobilePolicyValueInput from './MobilePolicyValueInput';
import MobileTermLengthInput from './MobileTermLengthInput';
import MobileQuoteDisplay from './MobileQuoteDisplay';
import {helpers} from '../BigQuoteBar.lifecyle';
import DownArrowIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import IconButton from 'material-ui/IconButton';
import styles from './MobileQuoteBar.styles';

/**
 * @class MobileQuoteBar
 * @param {object} props - see definition below
 * @classdesc
 * This is the Mobile version of the Quote Bar
 * */
class MobileQuoteBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = helpers.getInitialComponentState(props);
		this.onScrollEvent = this.onScrollEvent.bind(this);
	}

	componentDidMount() {
		window.addEventListener('scroll', this.onScrollEvent);
		this.onScrollEvent();
	}

	componentWillReceiveProps(props) {
		helpers.componentWillReceiveProps(props, this);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.onScrollEvent);
	}

	onScrollEvent() {
		const {drawerIsOpen, actions} = this.props;
		const scrollMax = 30;
		const scrollTop = pageYOffset;
		const hasScrolledPastLimit = (scrollTop > scrollMax);

		if (hasScrolledPastLimit && drawerIsOpen) {
			actions.mobileQuoteBarDrawerToggled();
		}
		if (hasScrolledPastLimit) {
			window.removeEventListener('scroll', this.onScrollEvent);
		}
	}

	render() {
		const {
			policyValue, termLength, drawerIsOpen, actions, currentSection,
			tollFreeNumber
		} = this.props;

		return (
			<Row className="mobileQuoteBar">
				<Col xs="12">
					<span>
						<MobileHeader
							searchTooltip="Click to talk to someone."
							tollFreeNumber={tollFreeNumber}
						/>
						<MobileQuoteDisplay
							currentSection={currentSection}
						/>
						<Row className={`mobileQuoteBar__drawer${drawerIsOpen ? ' mobileQuoteBar__drawer--open' : ' mobileQuoteBar__drawer--closed'}`}>
							<Col xs="12">
								<MobilePolicyValueInput
									policyValue={policyValue}
									handleHelperPopover={(event, popoverToShow) => helpers.handleHelperPopover(event, popoverToShow, this)}
									handleRequestClose={(popoverToClose) => helpers.handleRequestClose(popoverToClose, this)}
									updateState={(event) => helpers.updateState(event, this)}
									anchorEl={this.state.anchorEl}
									showPopover={this.state.showPopover.policyValue}
									onDragStop={actions.sliderDragStopped}
								/>
								<MobileTermLengthInput
									termLength={termLength}
									handleHelperPopover={(event, popoverToShow) => helpers.handleHelperPopover(event, popoverToShow, this)}
									handleRequestClose={(popoverToClose) => helpers.handleRequestClose(popoverToClose, this)}
									updateState={(event) => helpers.updateState(event, this)}
									anchorEl={this.state.anchorEl}
									showPopover={this.state.showPopover.termLength}
									onDragStop={actions.sliderDragStopped}
								/>
							</Col>
						</Row>
						<Row className={`mobileQuoteBar__iconWrapper${drawerIsOpen ? ' mobileQuoteBar__iconWrapper--drawerIsOpen' : ''}`}>
							<IconButton
								onClick={() => actions.mobileQuoteBarDrawerToggled()}
								style={styles.arrow.container}
								iconStyle={styles.arrow.icon}>
								<DownArrowIcon/>
							</IconButton>
						</Row>
					</span>
				</Col>
			</Row>
		);
	}
}

MobileQuoteBar.propTypes = {
	policyValue: React.PropTypes.number,
	actions: React.PropTypes.object,
	quoteAmountMonth: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]).isRequired,
	quoteAmountYear: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]).isRequired,
	termLength: React.PropTypes.number,
	drawerIsOpen: React.PropTypes.bool,
	tollFreeNumber: React.PropTypes.string,
	currentSection: React.PropTypes.number
};

export default MobileQuoteBar;
