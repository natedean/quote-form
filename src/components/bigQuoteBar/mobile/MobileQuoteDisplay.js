import React from 'react';
import Row from '../../common/Row';
import Col from '../../common/Col';

/**
 * @constant MobileQuoteDisplay
 * @param {object} props - Reference propTypes below
 * @description
 *   This displays the quote for the MobileQuotBar!
 * */
const MobileQuoteDisplay = (props) => {
	const {currentSection} = props;
	const stepNumberToShow = currentSection + 1;

	return (
		<div className="mobileQuoteDisplay">
			<Row>
				<Col xs="12" className="mobileQuoteDisplay__container">
					<Row>
						<Col xs="1"/>
						<Col xs="10">
							<div className="mobileQuoteDisplay__stepProgress">
								Step {stepNumberToShow} of 3
							</div>
						</Col>
					</Row>
					<Row>
						<Col xs="2"/>
						<Col xs="8">
							<p className="mobileQuoteDisplay__message">
								Keep going to see how much lower your rate can be.
							</p>
						</Col>
					</Row>
				</Col>
			</Row>
		</div>
	);
};

MobileQuoteDisplay.propTypes = {
	currentSection: React.PropTypes.number
};

export default MobileQuoteDisplay;
