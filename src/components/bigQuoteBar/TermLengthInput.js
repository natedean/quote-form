import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Slider from 'material-ui/Slider';
import termLengthFromInternal from '../../services/termLengthFromInternal';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styles from './BigQuoteBar.styles';
import links from '../../constants/textLinks';

/**
 * @const TermLengthInput
 * @param {object} props - see PropTypes below
 * @description
 * Creates the TermLengthInput slider along with tooltips and popovers
 */
const TermLengthInput = (props) => {
	const {
		termLength, handleHelperPopover, handleRequestClose, onDragStop,
		showPopover, anchorEl, updateState, quoteIsBeingRequested
	} = props;

	return (
		<div className="termLengthInput">
			<Row>
				<Col xs="12">
					<Row className="alignItems--center noMargin">
						<Col xs="6" className="termLengthInput__inputValue noPadding">
							{`${termLengthFromInternal(termLength).toFixed(0)} Years`}
						</Col>
						<Col xs="6" className="alignItems--flex-end noPadding">
							<Row className="alignItems--center">
								<div className="termLengthInput__inputLabel">
									<div>Choose</div>
									<div>term</div>
									<div>length</div>
								</div>

								<IconButton
									onMouseOver={(event) => handleHelperPopover(event, 'termLength')}
									style={styles.helpButton}
									iconStyle={styles.helpIcon}>
									<HelpIcon />
								</IconButton>

								<Popover
									style={styles.helpPopover}
									open={showPopover}
									anchorEl={anchorEl}
									anchorOrigin={styles.popoverAnchor}
									targetOrigin={styles.popoverTarget}
									onRequestClose={() => handleRequestClose('termLength')}
									className="popOver"
								>
									<div className="popOver__container">
										<p>
											More affordable than other types of insurance, a term life policy covers
											you for a specified period of time-usually 10, 20, or 30 years.
										</p>
									</div>
								</Popover>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
			<Row>
				<Col xs="12">
					<MuiThemeProvider muiTheme={styles.muiTheme}>
						<Slider
							name="termLength"
							disabled={quoteIsBeingRequested}
							className="termLengthInput__slider slider"
							value={termLength}
							min={.1}
							max={.3}
							step={.05}
							onDragStop={() => onDragStop()}
							onChange={(event, value) => {
								updateState({
									target: {
										name: 'termLength',
										value
									}
								});
							}}
						/>
					</MuiThemeProvider>
				</Col>
			</Row>
		</div>
	);
};

TermLengthInput.propTypes = {
	termLength: React.PropTypes.number,
	handleHelperPopover: React.PropTypes.func,
	handleRequestClose: React.PropTypes.func,
	updateState: React.PropTypes.func,
	anchorEl: React.PropTypes.object,
	showPopover: React.PropTypes.bool,
	quoteIsBeingRequested: React.PropTypes.bool,
	onDragStop: React.PropTypes.func
};

export default TermLengthInput;
