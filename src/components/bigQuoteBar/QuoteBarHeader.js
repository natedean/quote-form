import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import links from '../../constants/textLinks';

/* eslint-disable */
import logo from '../../images/SQ_logo.png';
/* eslint-enable */

/**
 * @const QuoteBarHeader
 * @param {object} props - see PropTypes below
 * @description
 * Creates the header with logo, message and phone number for desktop view.
 */
const QuoteBarHeader = (props) => {
	const {message, contactNumber} = props;

	return (
		<Row className="quoteBar__header alignItems--flex-end">
			<Col xs="6" className="noPadding">
				<div className="quoteBar__headerLogoWrapper">
					<a href={links.home}>
						<img className="quoteBar__headerLogo" src={logo}/>
					</a>
				</div>
			</Col>
			<Col xs="6" className="noPadding">
				<Row className="alignItems--center alignSelf--flex-end noMargin">
					<div className="quoteBar__headerMessage">{message}</div>
					<div className="quoteBar__headerContactNumber">{contactNumber}</div>
				</Row>
			</Col>
		</Row>
	);
};

QuoteBarHeader.propTypes = {
	message: React.PropTypes.string.isRequired,
	contactNumber: React.PropTypes.string.isRequired
};

export default QuoteBarHeader;
