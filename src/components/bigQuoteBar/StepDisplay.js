import React from 'react';
import Row from '../common/Row';
import Col from '../common/Col';
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import StepIcon from 'material-ui/svg-icons/image/lens';

/**
 *
 * @const StepDisplay
 * @type function
 * @description
 * This is the step display for the quote bar on desktop. Shows the step dot you are on along with the label for the step
 */
const StepDisplay = () => {
	return (
		<div className="stepDisplay">
			<Row className="stepDisplay__container noMargin">
				<Col xs="12" className="noPadding">
					<Stepper linear={false}>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Realize you need life insurance
								</div>
							</StepLabel>
						</Step>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Get a quote
								</div>
							</StepLabel>
						</Step>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Choose your company
								</div>
							</StepLabel>
						</Step>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Get a mini checkup
								</div>
							</StepLabel>
						</Step>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Get your underwriting results
								</div>
							</StepLabel>
						</Step>
						<Step completed={false}>
							<StepLabel
								icon={<StepIcon/>}
								className="stepDisplay__stepLabel">
								<div className="stepDisplay__stepText">
									Start your coverage
								</div>
							</StepLabel>
						</Step>
					</Stepper>
				</Col>
			</Row>
		</div>
	);
};

StepDisplay.propTypes = {};

export default StepDisplay;
