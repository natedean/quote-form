const getInitialComponentState = (props) => {
    return {
        loanInfo: {
            policyValue: props.policyValue,
            termLength: props.termLength
        },
        showPopover: {
            policyValue: false,
            termLength: false
        },
        anchorEl: {},
        drawerIsOpen: true,
        shouldBeSticky: false
    };
};

const updateState = (event, context) => {
    const field = event.target.name;
    let loanInfo = context.state.loanInfo;
    loanInfo[field] = event.target.value;

    if (event.target.name === 'policyValue') {
        context.props.actions.policyValueChanged(event.target.value);
    } else if (event.target.name === 'termLength') {
        context.props.actions.termLengthChanged(event.target.value);
    }

    context.setState({loanInfo});
};

const componentWillReceiveProps = (props, context) => {
    let loanInfo = context.state.loanInfo;

    if (props.termLength) {
        loanInfo.termLength = props.termLength;
    }

    if (props.policyValue) {
        loanInfo.policyValue = props.policyValue;
    }

    context.setState({loanInfo});
};

const handleHelperPopover = (event, popoverToShow, context) => {
    event.preventDefault();
    let showPopover = context.state.showPopover;
    showPopover[popoverToShow] = true;
    context.setState({showPopover, anchorEl: event.currentTarget});
};

const handleRequestClose = (popoverToClose, context) => {
    let showPopover = context.state.showPopover;
    showPopover[popoverToClose] = false;

    context.setState({showPopover, anchorEl: null});
};

const onToggleDrawer =(context) => {
    context.setState({drawerIsOpen: !context.state.drawerIsOpen});
};

export const helpers = {
    getInitialComponentState,
    updateState,
    componentWillReceiveProps,
    handleHelperPopover,
    handleRequestClose,
    onToggleDrawer
};
