import getMuiTheme from 'material-ui/styles/getMuiTheme';
let scssVars = require('../../styles/scss/base/variables/variables.json');

const defaults = {
	spacing: '15px',
	textAlign: 'middle',
	noSpace: '0px'
};

const muiTheme = getMuiTheme({
	slider: {
		trackSize: 2,
		trackColor: 'white',
		trackColorSelected: 'white',
		handleSize: 20,
		handleSizeDisabled: 8,
		handleSizeActive: 12,
		handleColorZero: 'white',
		handleFillColor: 'white',
		selectionColor: 'white',
		rippleColor: 'transparent',
	}
});

const styles = {
	muiTheme,
	classNames: {
		hideMobileQuoteBar: 'hideMobileQuoteBar',
		quoteBarLabels: 'quoteBarLabels',
		quoteBarInputs: 'quoteBarInputs',
		labelColumns: 'labelColumns',
		policyInput: 'policyInput',
		termInput: 'termInput',
		estimateLabel: 'estimateLabel'
	},
	overlay: {
		backgroundColor: 'transparent'
	},
	container: {
		marginLeft: defaults.noSpace,
		marginRight: defaults.noSpace,
	},
	column: {
		paddingLeft: defaults.noSpace,
		paddingRight: defaults.noSpace
	},
	inputsContainer: {
		height: '190px',
		marginTop: '10px'
	},
	backgroundBanner: {
		left: 0,
		width: '100%',
	},
	labelColumns: {
		marginTop: defaults.spacing
	},
	helpButton: {
		padding: 0
	},
	helpIcon: {
		width: 27,
		height: 27
	},
	helpPopover: {
		marginTop: defaults.spacing,
		marginLeft: '175px',
		width: '500px',
		fontSize: '1.4em',
		borderRadius: defaults.spacing,
		border: `2px solid ${scssVars.colors.orange}`
	},
	popoverAnchor: {
		horizontal: defaults.textAlign,
		vertical: 'bottom'
	},
	popoverTarget: {
		horizontal: defaults.textAlign,
		vertical: 'top'
	},
	componentDivider: {
		position: 'absolute',
		top: '45px',
		left: '87.5%',
		height: '125px',
		borderLeft: '1px solid white'
	},
	quoteDisplay: {
		yearLabel: {
			color: '#FFB74D',
		}
	},
};

export default styles;
