import React from 'react';

/**
 * @function ErrorText
 * @param {object} props
 * @description
 *   This component is responsible for displaying Error Text for Inputs
 * */
const ErrorText = (props) => {
	const {showErrorText, errorText} = props;

	return (
		<div className={showErrorText ? 'color--pink errorText' : 'invisible errorText'}>
			{errorText}
		</div>
	);
};

ErrorText.propTypes = {
	showErrorText: React.PropTypes.bool,
	errorText: React.PropTypes.string
};

export default ErrorText;
