import React from 'react';
import ArrowIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import Select from 'react-select';
import styles from './CustomSelectField.styles';

/**
 * @function CustomSelectField
 * @param {object} props
 * @description
 *   This is a custom Select Field that wraps react-select component for our uses.
 * */
const CustomSelectField = (props) => {
	const {value, placeholder, collection, name, updateState, onOpen, onClose, disabled}
		= props;
	return (
		<div>
			<Select
				onOpen={onOpen}
				onClose={onClose}
				value={value}
				placeholder={placeholder}
				name={name}
				size={collection.length}
				clearable={false}
				disabled={disabled}
				onChange={(newValue) => updateState(newValue)}
				searchable={false}
				options={props.collection}
				style={styles.select}
			/>
			<div name="outerBox" style={styles.selectWrapper}>
				<div
					style={styles.iconWrapper}
				>
					<ArrowIcon />
				</div>
			</div>
		</div>
	);
};

CustomSelectField.propTypes = {
	updateState: React.PropTypes.func,
	value: React.PropTypes.oneOfType([
		React.PropTypes.number,
		React.PropTypes.string
	]),
	placeholder: React.PropTypes.string,
	collection: React.PropTypes.array.isRequired,
	name: React.PropTypes.string.isRequired,
	onOpen: React.PropTypes.func,
	onClose: React.PropTypes.func,
	disabled: React.PropTypes.bool
};

export default CustomSelectField;
