import React from 'react';

/**
 * @const Row
 * @param {object} props - see PropTypes below
 * @description
 * Returns a div with the class row that uses the flex grid layout
 * @example
 * <Row className="someClassName"></Row>
 */
const Row = (props) => {
    const {className, children} = props;
    const classes = className ? className : '';

    return (
        <div className={`row ${classes}`}>{children}</div>
    );
};

Row.propTypes = {
    children: React.PropTypes.node,
    className: React.PropTypes.string
};

export default Row;
