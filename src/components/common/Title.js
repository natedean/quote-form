import React from 'react';

/**
 * @function Title
 * @param {object} props
 * @description
 *   This component is intended to handle the Input Titles and their interactivity
 * */
const Title = (props) => {
	const {titleText, className} = props;

	return (
		<h4 className={className}>
			<span>
				{titleText}
			</span>
		</h4>
	);
};

Title.propTypes = {
	titleText: React.PropTypes.string,
	className: React.PropTypes.string
};

export default Title;
