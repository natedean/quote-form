import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import styles from './StepButton.styles';

/**
 * @const StepButton
 * @param {object} props - see PropTypes
 * @description
 * A reusable next/prev button to navigate the different sections of the Life app
 */
const StepButton = (props) => {
	const {updateSection, label, disabled} = props;
	return (
		<div>
			<FlatButton
				disabled={disabled}
				style={styles.button}
				labelStyle={styles.label}
				onClick={updateSection}
				label={label}
			/>
		</div>
	);
};

StepButton.propTypes = {
	updateSection: React.PropTypes.func,
	label: React.PropTypes.string,
	disabled: React.PropTypes.bool
};

export default StepButton;
