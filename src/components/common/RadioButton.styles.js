let scssVars = require('../../styles/scss/base/variables/variables.json');

const RadioButtonStyles = {
	radioButton: {
		width: '25%'
	},
	checkedIcon: {
		fill: scssVars.colors.orange,
		height: '34px',
		width: '34px',
		marginLeft: '-5px',
		marginTop: '-5px'
	},
	uncheckedIcon: {
		fill: scssVars.colors.whiteGray,
		height: '34px',
		width: '34px',
		marginLeft: '-5px',
		marginTop: '-5px'
	},
	radioButtonIcon: {
		backgroundColor: scssVars.colors.whiteGray,
		padding: '5px',
		borderRadius: '34px',
		height: '34px',
		width: '34px',
		marginRight: 0
	},
	radioButtonLabel: {
		color: 'black',
		fontSize: '19px',
		paddingLeft: '10px',
		paddingRight: '10px',
		paddingTop: '8px'
	}
};

export default RadioButtonStyles;
