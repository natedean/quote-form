let scssVars = require('../../styles/scss/base/variables/variables.json');

const styles = {
	button: {
		width: '190px',
		height: 'auto',
		maxHeight: '40px',
		borderRadius: '50px',
		margin: 'auto',
		paddingBottom: '2px',
		backgroundColor: scssVars.colors.orange,
		hoverColor: scssVars.colors.orange
	},
	label: {
		fontSize: '18px',
		textAlign: 'center',
		textTransform: 'none',
		fontWeight:'600'
	}
};

export default styles;
