let scssVars = require('../../styles/scss/base/variables/variables.json');

const styles = {
	select: {
		height: '48px',
		width: '100%',
		border: 'none',
		borderRadius: '6px',

		color: 'grey',
		backgroundColor: scssVars.colors.whiteGray,

		fontSize: '16px',
		lineHeight: '24px',
	},
	selectWrapper: {
		position: 'absolute',
		top: 0,
		right: 0,

		display: 'inline-block',
		width: '34px',
		marginLeft: '-34px',
		borderBottomRightRadius: '6px',
		borderTopRightRadius: '6px',

		backgroundColor: scssVars.colors.harvest,

		lineHeight: '32px',

		cursor: 'pointer',
		pointerEvents: 'none',
	},
	iconWrapper: {
		paddingTop: '15px',
		paddingLeft: '5px'
	}
};

export default styles;
