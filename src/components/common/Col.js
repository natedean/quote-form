import React from 'react';

/**
 * @const Col
 * @param {object} props - see PropTypes below
 * @description
 * Returns div with flex grid properties that are passed to it.
 * @example
 * <Col xs="12" sm="6" md="4" lg="3" className="someClassName"></Col>
 */
const Col = (props) => {
    const {xs, sm, md, lg, className, children} = props;
    const extraSmall = xs ? `col-xs-${xs} ` : '';
    const small = sm ? `col-sm-${sm} ` : '';
    const medium = md ? `col-md-${md} ` : '';
    const large = lg ? `col-lg-${lg} ` : '';
    const classes = className ? className : '';

    return (
        <div className={extraSmall + small + medium + large + classes}>{children}</div>
    );
};

Col.propTypes = {
    children: React.PropTypes.node,
    className: React.PropTypes.string,
    xs: React.PropTypes.string,
    sm: React.PropTypes.string,
    md: React.PropTypes.string,
    lg: React.PropTypes.string
};

export default Col;
