import React from 'react';
import LensIcon from 'material-ui/svg-icons/image/lens';
import radioStyles from './RadioButton.styles';

const checkedIcon = (<LensIcon style={radioStyles.checkedIcon}/>);
const uncheckedIcon = (<LensIcon style={radioStyles.uncheckedIcon}/>);

const icons = {
	checkedIcon,
	uncheckedIcon
};

export default icons;
