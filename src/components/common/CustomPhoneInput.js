import React from 'react';
import {bindAll} from 'lodash';
import scrollToInput from '../../services/scrollToInput';

/**
 * @class CustomPhoneInput
 * @description
 *   This class is going to manage state for the Custom input happenings
 * */
class CustomPhoneInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			phoneNumber: props.phoneNumber,
			lastCursorPosition: 0
		};

		bindAll(this, ['updateState']);
	}


	updateState(event) {
		const {updateState} = this.props;
		const currentValue = event.target.value;
		const isANumber = /^[0-9]*$/.test(currentValue);

		if (isANumber) {
			// 1.) Then update our component so it is "NSYNC" with the Redux store
			this.setState({phoneNumber: currentValue});

			// 2.) Lastly, send the newly masked phoneNumber to Redux
			updateState({
				target: {
					value: currentValue,
					name:event.target.name
				}
			});
		} else {
			event.preventDefault();
		}
	}

	render() {
		const {phoneNumber} = this.state;
		return (
			<div className="customPhoneInput">
				<input
					name="phoneNumber"
					ref="phoneInput"
					value={phoneNumber}
					className="customPhoneInput__input"
					type="tel"
					placeholder="0000000000"
					onChange={(event) => this.updateState(event)}
					onFocus={(event) => scrollToInput({target: {name: 'phoneNumber', offsetParent: event.target.parentElement}})}
					maxLength="10"
				/>
			</div>
		);
	}
}

CustomPhoneInput.propTypes = {
	updateState: React.PropTypes.func,
	phoneNumber: React.PropTypes.string,
	scrollToPhoneInput: React.PropTypes.func
};

export default CustomPhoneInput;
