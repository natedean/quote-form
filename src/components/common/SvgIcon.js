import React from 'react';
/**
 * @const SvgIcon
 * @param {array} props.drawPaths - array listing path descriptions to be drawn
 * @param {array} props.drawPolygons - array listing points to draw a polygon between
 * @param {string} props.svgWrapperClass - class for overall svgWrapper
 * @param {string} props.svgIconClass - class for svgIcons, specifically used for color
 * @param {string} props.viewBox - string specifying svg viewbox attribute, if necessary
 * @description
 * Used to dynamically draw SVG's and modify different attributes dynamically
 */

const SvgIcon = (props) => {
	const {drawPaths, drawPolygons, svgWrapperClass, svgIconClass, viewBox} = props;

	//Build path children based on saved 'd' constants. Path supplied as key for react dom
	let domPaths = [];
	if(drawPaths){
		domPaths = drawPaths.map(path =>
			<path className={svgIconClass} key={path} d={path}/>
		);
	}
	if(drawPolygons) {
		domPaths = [...domPaths, drawPolygons.map(polygon =>
			<polygon className={svgIconClass} key={polygon} points={polygon}/>
		)];
	}

	return (
		<svg className={svgWrapperClass}
			viewBox={viewBox}>
			{domPaths}
		</svg>
	);
};

export default SvgIcon;

SvgIcon.propTypes = {
	drawPaths: React.PropTypes.array,
	drawPolygons: React.PropTypes.array,
	svgWrapperClass: React.PropTypes.string,
	svgIconClass: React.PropTypes.string,
	viewBox: React.PropTypes.string
};
