const getInitialComponentState = (props) => {
    return {
        submitState: {
            isEntryReadyToSubmit: props.isEntryReadyToSubmit,
            currentFormEntry: props.currentFormEntry
        }
    };
};

const componentWillReceiveProps = (props, context) => {
    let submitState = context.state.submitState;

    if (props.isEntryReadyToSubmit) {
        submitState.isEntryReadyToSubmit = props.isEntryReadyToSubmit;
    }
    if (props.currentFormEntry) {
        submitState.currentFormEntry = props.currentFormEntry;
    }

    //TODO Call ValidFormCheck action with currentFormEntry

    context.setState({submitState});
};

export const helpers = {
    getInitialComponentState,
    componentWillReceiveProps
};