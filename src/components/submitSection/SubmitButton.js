import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Row from '../common/Row';
import Col from '../common/Col';
import styles from '../common/StepButton.styles';
import CircularProgress from 'material-ui/CircularProgress';

/**
 * @function SubmitButton
 * @param {object} props
 * @description
 *  This is responsible for displaying a button and firing the Submission action
 * */
const SubmitButton = (props) => {

	const {
		formIsSubmitting, submitForm, submissionErrorMessage,
		quoteIsBeingRequested, showCurrentSectionErrors
	} = props;

	return (
		<Row>
			<Col xs="12">
				<div className="text--center">
					<FlatButton
						style={styles.button}
						labelStyle={styles.label}
						onClick={(event) => submitForm()}
						disabled={showCurrentSectionErrors || formIsSubmitting || quoteIsBeingRequested}
						label={!formIsSubmitting && "Submit"}
					>
						{formIsSubmitting && <CircularProgress size={0.5} style={{ top: '-0.3rem' }} />}
					</FlatButton>
					{!formIsSubmitting && <div className="color--pink">
						<div>{submissionErrorMessage}</div>
					</div>}
				</div>
			</Col>
		</Row>
	);
};

SubmitButton.propTypes = {
	isEntryReadyToSubmit: React.PropTypes.bool,
	submitForm: React.PropTypes.func,
	formIsSubmitting: React.PropTypes.bool,
	quoteIsBeingRequested: React.PropTypes.bool,
	submissionErrorMessage: React.PropTypes.string,
	showCurrentSectionErrors: React.PropTypes.bool
};

export default SubmitButton;
