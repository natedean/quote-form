import React from 'react';
import {Link} from 'react-router';
import Row from '../common/Row';
import Col from '../common/Col';
import textLinks from '../../constants/textLinks';

/**
 * @function CarrierDisclaimerLinks
 * @param {object} props - properties this component needs to work correctly; currently none
 * @description
 *   This component is responsible for displaying 2 carrier Policy links
 * */
const CarrierDisclaimerLinks = (props) => {
	return (
		<Row>
			<Col xs="12" className="mt-2">
				<div className="text--center">
					<Link className="color--stone pl-5px pr-5px" to={textLinks.legal.disclaimers} target="_blank">
						Carriers & Policies
					</Link>
					|
					<Link className="color--stone pl-5px pr-5px" to={`${textLinks.legal.disclaimers}?state=fl`} target="_blank">
						Carriers & Policies - Florida
					</Link>
				</div>
			</Col>
		</Row>
	);
};

export default CarrierDisclaimerLinks;
