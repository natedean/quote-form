import React from 'react';
import SubmitButton from './SubmitButton';
import CarrierDisclaimerLinks from './CarrierDisclaimerLinks';
import Row from '../common/Row';
import Col from '../common/Col';
import {helpers} from './SubmitSection.lifecycle';
import maskPhoneNumber from '../../services/maskPhoneNumber';
import StepButton from '../common/StepButton';

export default class SubmitSection extends React.Component {

	constructor(props) {
		super(props);

		this.state = helpers.getInitialComponentState(props);
	}

	componentWillReceiveProps(props) {
		helpers.componentWillReceiveProps(props, this);
	}

	render() {
		const {
			isEntryReadyToSubmit, formIsSubmitting, showCurrentSectionErrors,
			quoteIsBeingRequested, submissionErrorMessage, actions, tollFreeNumber
		} = this.props;
		const nextSectionIndex = 1;

		return (
			<div className="submitSection contentContainer">
				<Row>
					<Col xs="12" sm="6" className="text--center mb-2">
						<StepButton
							updateSection={(event) => actions.updateSection(nextSectionIndex)}
							label="Previous"
						/>
					</Col>
					<Col xs="12" sm="6">
						<SubmitButton
							submissionErrorMessage={submissionErrorMessage}
							formIsSubmitting={formIsSubmitting}
							quoteIsBeingRequested={quoteIsBeingRequested}
							isEntryReadyToSubmit={isEntryReadyToSubmit}
							showCurrentSectionErrors={showCurrentSectionErrors}
							submitForm={actions.formSubmitted}
						/>
					</Col>
					<Col xs="12">
						<div className="text--center color--stone mt-4">
							By Clicking the "Submit" button above and submitting your online quote request to SelectQuote, you are agreeing
							by your electronic signature to give SelectQuote, as well as their partners and independent contractors, your prior express
							written consent to call or text you at the phone number provided in your quote online request form using our automatic dialing
							system and pre-recorded [or artificial voice] messages to market our products and services to you and for any other purpose. Your
							consent is not required to get a quote or purchase anything from SelectQuote, and you may instead reach us by phone at
							<span className="submitSection__phoneContainer"><a href={`tel:${tollFreeNumber}`} className="submitSection__phone">{maskPhoneNumber(tollFreeNumber, true)}</a></span>. We do not and will not sell your personal information to other companies.
						</div>
						<CarrierDisclaimerLinks />
					</Col>
				</Row>
			</div>
		);
	}
}

SubmitSection.propTypes = {
	isEntryReadyToSubmit: React.PropTypes.bool,
	currentFormEntry: React.PropTypes.object,
	actions: React.PropTypes.object,
	disclaimerText: React.PropTypes.string,
	formIsSubmitting: React.PropTypes.bool,
	quoteIsBeingRequested: React.PropTypes.bool,
	submissionErrorMessage: React.PropTypes.string,
	incompleteFields: React.PropTypes.object,
	showCurrentSectionErrors: React.PropTypes.bool,
	tollFreeNumber: React.PropTypes.string
};
