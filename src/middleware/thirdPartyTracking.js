import actionTypes from '../actions/AppTypes';

let currentSection;
let sectionsAttained = [];
let hasFormSubmittedSuccessfully = false;

/**
 * @function setCurrentSectionTracking
 * @param {number} val - The incoming section value from the store
 * @description
 *   This function tracks changes in the currentSection and sectionsAttained variables. It will call
 *   window._satellite.track() when the user navigates to a new "fresh" section
 * */
const setCurrentSectionTracking = (val) => {
	const prevSection = currentSection;

	currentSection = val;

	if (currentSection === prevSection) return;

	if (currentSection > prevSection && !sectionsAttained.includes(currentSection)) {
		sectionsAttained = [...sectionsAttained, currentSection];
		window._satellite.track(`Request_Newform_step${currentSection}`);
	}
};

/**
 * @function setHasFormCompleted
 * @param {bool} action - An action
 * @description
 *     Upon FORM_SUBMISSION_SUCCESS we are setting the requestQuote.referencenumbernew property to the referencenumber
 *   returned by the api. We are also calling window._satellite.track() with the appropriate string.
 * */
const setHasFormCompleted = (action) => {
	if (hasFormSubmittedSuccessfully || action.type !== actionTypes.FORM_SUBMISSION_SUCCESS) return;

	hasFormSubmittedSuccessfully = true;
	window.digitalData.requestQuote.referencenumbernew = _extractReferenceNumberFromAction(action);
	window._satellite.track(`Request_Newform_completes`);
};

/**
 * @function thirdPartyTracking
 * @param {object} store
 * @description
 *   This middleware will run side-effecty tracking methods if/when store properties change.
 * @return {func} Returns a function that returns a function that will receive an
 * action. The last function returns the new state.
 * */
const thirdPartyTracking = store => next => action => {
	const result = next(action);
	// if the global tracking objects are not in place, don't bother
	// we will set this here, as opposed to before subscribe, just in case there is a load-order or timing issue
	if (!window.digitalData || !window.digitalData.requestQuote || !window._satellite) { return result; }

	const currentState = store.getState();

	setCurrentSectionTracking(_currentSectionSelector(currentState));
	setHasFormCompleted(action);

	return result;
};

export default thirdPartyTracking;

/**
 * @function _currentSectionSelector
 * @param {object} state - The result of calling store.getState()
 * @description
 *   This is a selector, common in redux.
 * @return {number} App.currentSection from the store
 * */
function _currentSectionSelector(state) { return state.App.currentSection; }

/**
 * @function _extractReferenceNumberFromAction
 * @param {object} action - An action
 * @param {string} action.type - Type of action
 * @param {object} action.result - The result of the Successful Form Submission api call.
 * @description
 *   This deeply nested referencenumber property looks fragile. Wrap it in a try/catch so it doesn't blow up if the
 *   path changes.
 * @return {string} The referencenumber of the successful form submission or a generic string.
 * */
function _extractReferenceNumberFromAction(action) {
	try {
		return action.result.reply.status.referencenumber;
	} catch (err) {
		return 'There is no referencenumber, or the api response shape has changed.';
	}
}
