import AppActions from '../actions';

import React from 'react';
import SubmitSection from '../components/submitSection/SubmitSection';
import {connect} from 'react-redux';

const mapStateToProps = state => {
	return {
		currentFormEntry: state.App.currentFormEntry,
		isEntryReadyToSubmit: state.App.isEntryReadyToSubmit,
		formIsSubmitting: state.App.formIsSubmitting,
		submissionErrorMessage: state.App.submissionErrorMessage,
		quoteIsBeingRequested: state.App.quoteIsBeingRequested,
		showCurrentSectionErrors: state.App.showCurrentSectionErrors,
		tollFreeNumber: state.App.tollFreeNumber
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {
			formSubmitted: (currentFormEntry) => {
				dispatch(AppActions.FormSubmitted(currentFormEntry));
			},
			validFormCheck: (currentFormEntry) => {
				dispatch(AppActions.ValidFormCheck(currentFormEntry));
			},
			updateSection: (sectionNumber) => {
				dispatch(AppActions.UpdateSection(sectionNumber));
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SubmitSection);
