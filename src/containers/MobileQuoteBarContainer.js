import AppActions from '../actions';

import React from 'react';
import MobileQuoteBar from '../components/bigQuoteBar/mobile/MobileQuoteBar';
import {connect} from 'react-redux';

const mapStateToProps = state => {
	return {
		currentSection: state.App.currentSection,
		termLength: state.App.currentFormEntry.termLength,
		policyValue: state.App.currentFormEntry.policyValue,
		quoteAmountMonth:state.App.quoteAmountMonth,
		quoteAmountYear: state.App.quoteAmountYear,
		drawerIsOpen: state.App.drawerIsOpen,
		quoteIsBeingRequested: state.App.quoteIsBeingRequested,
		tollFreeNumber: state.App.tollFreeNumber
	};
};

const mapDispatchToProps = dispatch => {

	return {
		actions: {
			policyValueChanged: (policyValue) =>  {
				dispatch(AppActions.PolicyValueChanged(policyValue));
			},
			termLengthChanged: (termLength) =>  {
				dispatch(AppActions.TermLengthChanged(termLength));
			},
			mobileQuoteBarDrawerToggled: () => {
				dispatch(AppActions.MobileQuoteBarDrawerToggled());
			},
			sliderDragStopped: () =>  {
				dispatch(AppActions.SliderDragStopped());
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MobileQuoteBar);
