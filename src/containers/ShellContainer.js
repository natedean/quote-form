import AppActions from '../actions';
import React from 'react';
import App from '../components/App';
import {connect} from 'react-redux';

const mapStateToProps = state => {
	return {
		redirectLink: state.App.redirectLink
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {
			maskPhoneNumber: (phoneNumber) => {
				dispatch(AppActions.MaskPhoneNumber(phoneNumber));
			},
			policyValueChanged: (policyValue) => {
				dispatch(AppActions.PolicyValueChanged(policyValue));
			},
			termLengthChanged: (termLength) => {
				dispatch(AppActions.TermLengthChanged(termLength));
			},
			cidChanged: (cid) => {
				dispatch(AppActions.CidChanged(cid));
			},
			sourceChanged: (source) => {
				dispatch(AppActions.SourceChanged(source));
			},
			tollFreeNumberChanged: (tollFreeNumber) => {
				dispatch(AppActions.TollFreeNumberChanged(tollFreeNumber));
			},
			trustPilotCustomersChanged: (trustPilotCustomers) => {
				dispatch(AppActions.TrustPilotCustomersChanged(trustPilotCustomers));
			},
			participantChanged: (participant) => {
				dispatch(AppActions.ParticipantChanged(participant));
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
