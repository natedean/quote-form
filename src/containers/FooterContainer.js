import React from 'react';
import Footer from '../components/footer/Footer';
import {connect} from 'react-redux';

const mapStateToProps = state => {
	return {
		trustPilotCustomers: state.App.trustPilotCustomers,
		tollFreeNumber: state.App.tollFreeNumber
	};
};

export default connect(mapStateToProps)(Footer);

