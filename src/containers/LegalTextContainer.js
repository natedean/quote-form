import React from 'react';
import LegalText from '../components/footer/LegalText';
import {connect} from 'react-redux';

const mapStateToProps = state => {
	return {
		policyName: state.App.policyName,
		carrierID: state.App.carrierID
	};
};

export default connect(mapStateToProps, null)(LegalText);
