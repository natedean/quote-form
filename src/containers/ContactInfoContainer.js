import AppActions from '../actions';

import React from 'react';
import ContactInfo from '../components/contactInfo/ContactInfo';
import {connect} from 'react-redux';
import constants from '../constants/generalConstants';

const mapStateToProps = state => {
	return {
		currentFormEntry: state.App.currentFormEntry,
		headerText: constants.contactHeader,
		subHeaderText: constants.contactSubHeader,
		phoneErrorText: state.App.errorTextCollection.phoneNumber,
		emailErrorText: state.App.errorTextCollection.email,
		firstNameErrorText: state.App.errorTextCollection.firstName,
		lastNameErrorText: state.App.errorTextCollection.lastName,
		drawerIsOpen: state.App.drawerIsOpen,
		showCurrentSectionErrors: state.App.showCurrentSectionErrors
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {
			firstNameChanged: (event) =>  {
				dispatch(AppActions.FirstNameChanged(event));
			},
			lastNameChanged: (event) =>  {
				dispatch(AppActions.LastNameChanged(event));
			},
			emailChanged: (event) =>  {
				dispatch(AppActions.EmailChanged(event));
			},
			phoneNumberChanged: (event) =>  {
				dispatch(AppActions.PhoneNumberChanged(event));
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo);
