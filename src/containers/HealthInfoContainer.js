import AppActions from '../actions';

import React from 'react';
import HealthInfo from '../components/healthInfo/HealthInfo';
import {connect} from 'react-redux';
import constants from '../constants/generalConstants';

const mapStateToProps = state => {
	return {
		currentFormEntry: state.App.currentFormEntry,
		headerText: constants.healthHeader,
		subHeaderText: constants.subHeaderText,
		quoteIsBeingRequested: state.App.quoteIsBeingRequested,
		drawerIsOpen: state.App.drawerIsOpen,
		incompleteFields: state.App.incompleteFields,
		healthRatingChanged: state.App.healthRatingChanged,
		showCurrentSectionErrors: state.App.showCurrentSectionErrors
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {
			healthRatingChanged: (event) =>  {
				dispatch(AppActions.HealthRatingChanged(event));
			},
			hasUsedNicotineChanged: (event) =>  {
				dispatch(AppActions.HasUsedNicotineChanged(event));
			},
			weightChanged: (event) =>  {
				dispatch(AppActions.WeightChanged(event));
			},
			heightChanged: (event) =>  {
				dispatch(AppActions.HeightChanged(event));
			},
			updateSection: (sectionNumber) => {
				dispatch(AppActions.UpdateSection(sectionNumber));
			},
			sliderDragStopped: () =>  {
				dispatch(AppActions.SliderDragStopped());
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(HealthInfo);
