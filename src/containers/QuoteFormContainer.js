import React from 'react';
import {connect} from 'react-redux';
import QuoteFormContainer from '../components/QuoteForm';


const mapStateToProps = state => {
	return {
		currentSection: state.App.currentSection,
		drawerIsOpen: state.App.drawerIsOpen
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(QuoteFormContainer);
