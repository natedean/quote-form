import AppActions from '../actions';

import React from 'react';
import BasicInfo from '../components/basicInfo/BasicInfo';
import {connect} from 'react-redux';
import constants from '../constants/generalConstants';

const mapStateToProps = state => {
	return {
		currentFormEntry: state.App.currentFormEntry,
		headerText: constants.basicHeader,
		subHeaderText: constants.subHeaderText,
		zipcodeErrorText: state.App.errorTextCollection.zipcode,
		drawerIsOpen: state.App.drawerIsOpen,
		quoteIsBeingRequested: state.App.quoteIsBeingRequested,
		showCurrentSectionErrors: state.App.showCurrentSectionErrors
	};
};

const mapDispatchToProps = dispatch => {
	return {
		actions: {
			hasCurrentPolicyChanged: (event) =>  {
				dispatch(AppActions.HasCurrentPolicyChanged(event));
			},
			genderChanged: (event) =>  {
				dispatch(AppActions.GenderChanged(event));
			},
			zipcodeChanged: (event) =>  {
				dispatch(AppActions.ZipcodeChanged(event));
			},
			birthdateChanged: (event) =>  {
				dispatch(AppActions.BirthdateChanged(event));
			},
			updateSection: (sectionNumber) => {
				dispatch(AppActions.UpdateSection(sectionNumber));
			}
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(BasicInfo);
