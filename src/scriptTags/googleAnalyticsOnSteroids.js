/*eslint-disable */
const GAS = () => {
	var _gas = _gas || [];
	// TODO: Update ID
	_gas.push(['_setAccount', 'UA-2081734-17']); // REPLACE WITH YOUR GA NUMBER
	_gas.push(['_setDomainName', 'selectquote.com']); // REPLACE WITH YOUR DOMAIN
	_gas.push(['_gasTrackVimeo', { force: true }]);

	var ga = document.createElement('script');
	ga.id = 'gas-script';
	ga.setAttribute('data-use-dcjs', 'false'); // CHANGE TO TRUE FOR DC.JS SUPPORT
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = '/Scripts/gas.min.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
};
export default GAS;
/*eslint-enable */
