/*eslint-disable */
const getDigitalData = () => ({
	pageInfo: {
		pageName: "Quote Form",
		server: "quoteform.selectquote.com",
		region: "North America",
		country: "USA",
		siteSection: "Home",
		SiteSubsection1: "n/a",
		SiteSubsection2: "n/a"
	},
	formData: {
		formName: "SelectQuote Quote Form v2.0"
	},
	requestQuote: {
		formnamenew: "requestQuote New"
	}
});
export default getDigitalData;
/*eslint-enable */
